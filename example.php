<?php
/**
 * Pygments for PHP is a PHP-based wrapper to some of the Python Pygments API via the command-line pygmentize script.
 *
 *
 * ---------------------- Use this example file to get a jump-start on using the Pygments class. ----------------------
 *
 *
 * @date 2013-02-05
 * @copyright Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 *
 * Bitbucket URL: http://bitbucket.org/markswhitemedia/pygments-for-php
 *
 * BSD 3-Clause License
 *
 * Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 *      disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * - Neither the name of the Michael White nor the names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



/*
 * Use this example file to get a jump-start on using the Pygments class.
 */

require_once('load-pygments.php');

// Obtain a snippet of code for testing purposes.
$htmlCodeSample = <<<CODE
<html>
    <body>
        <p>Hi there <strong>Pygments</strong>!</p>
    </body>
</html>
CODE;

// Set the lexer name we want to use. In this case, we are trying to highlight some HTML code.
$lexerName = 'html';

// Set the formatter name we want to use. In this case, we are rendering the highlighted code inside an HTML document.
$formatterName = 'html';

// Get an instance of the Pygments class.
$pygments = new Pygments();

// Set a couple of options.
$pygments->setOptions(array(
    'linenos' => 'table', // Render line numbers in a table cell.
    'full' => true // Render a full HTML document. (By default it just renders as a snippet of HTML you can drop into a page)
));

// Get the highlighted code!
$output = $pygments->highlight($htmlCodeSample, $lexerName, $formatterName);

// Now for the easy part, render the page as an HTML document.
header('Content-Type: text/html');
echo $output;