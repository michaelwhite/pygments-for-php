
Version: 1.0rc1

Requires PHP 5.3+

See the pygmentize.php script for an in-depth example of how to use the Pygments class.


Simple Example:

<?php

// Highlights some snippet of CSS code using the HTML formatter

$cssCode = <<<CODE
body {
    font-family: Arial, Verdana, sans-serif;
    color: #333333;
    font-size: 14px;
}
CODE;

$p = new Pygments();
$html = $p->highlight($cssCode, 'css', 'html');

echo $html;