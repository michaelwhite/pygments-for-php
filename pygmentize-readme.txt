
API Version 1.0.rc1
The API version number is independent from this library as a whole.

This is a readme file for the pygmentize.php script only. That script operates as an API service for loading highlighted
code blocks.

Currently the request can be made via GET or POST. Using POST is recommended to avoid maximum URI length issues that can
occur when sending code via a GET request.

Request Parameters:
@param string $lexer Any valid lexer name that Pygments recognizes.
@param string $code The code to highlight with the specified lexer.

Example GET Request URL:

/pygments-for-php/pygmentize.php?lexer=css&code=body { color: #333333; }



Response Information:
Return format is JSON

Response Parameters:
@param string $result

Example JSON Response Object (for the example request above)

{
    "result": "<table class=\"highlighttable\"><tr><td class=\"linenos\"><div class=\"linenodiv\"><pre>1<\/pre><\/div><\/td><td class=\"code\"><div class=\"highlight\"><pre><span class=\"nt\">body<\/span> <span class=\"p\">{<\/span> <span class=\"k\">color<\/span><span class=\"o\">:<\/span> <span class=\"m\">#333333<\/span><span class=\"p\">;<\/span> <span class=\"p\">}<\/span>\n<\/pre><\/div>\n<\/td><\/tr><\/table>"
}