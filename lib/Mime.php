<?php
/**
 * Pygments for PHP is a PHP-based wrapper to some of the Python Pygments API via the command-line pygmentize script.
 *
 * @date 2013-02-05
 * @copyright Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 *
 * Bitbucket URL: http://bitbucket.org/markswhitemedia/pygments-for-php
 *
 * BSD 3-Clause License
 *
 * Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 *      disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * - Neither the name of the Michael White nor the names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Pygments;

/**
 * A class designed to allow for easy look-ups of mime-types based on file extensions.
 */
class Mime {
    /**
     * A list of mime types and their associated files extension(s).
     *
     * @var array
     */
    protected static $_mimeTypes = false;

    protected static $_extensionTypes = false;

    protected static $_textExceptions = array(
        'application/x-javascript'
    );

    /**
     * Loads the mime type definitions from a special configuration file.
     */
    protected static function _loadMimeTypes() {
        if(!self::$_mimeTypes) {
            $typesFile = realpath(dirname(__FILE__).'/Mime/mime-types.php');
            if($typesFile) {
                self::$_mimeTypes = include_once($typesFile);
            }
        }
    }

    protected static function _loadExtensionTypes() {
        if(!self::$_extensionTypes) {
            $typesFile = realpath(dirname(__FILE__).'/Mime/extension-types.php');
            if($typesFile) {
                self::$_extensionTypes = include_once($typesFile);
            }
        }
    }

    public static function getFileExtension($path, $includeDot=false) {
        return (string) ($includeDot ? strrchr($path, '.') : substr(strrchr($path, '.'), 1));
    }

    /**
     * Retrieves the mime type of the source file based on the file extension.
     *
     * @param string $sourcePath The path to the file to get the mime-type of.
     */
    public static function getType($sourcePath) {
        if(!self::$_extensionTypes) {
            self::_loadExtensionTypes();
        }
        $ext = strtolower(self::getFileExtension(basename($sourcePath), true));
        if(isset(self::$_extensionTypes[$ext]['mimeTypes'][0])) {
            return self::$_extensionTypes[$ext]['mimeTypes'][0];
        }
        return false;
    }

    public static function getLabelByMimeType($mimeType) {
        if(!self::$_mimeTypes) {
            self::_loadMimeTypes();
        }
        $mimeType = strtolower($mimeType);
        if(isset(self::$_mimeTypes[$mimeType]['label'])) {
            return self::$_mimeTypes[$mimeType]['label'];
        }
        return false;
    }

    public static function getExtensionByMimeType($mimeType) {
        if(!self::$_mimeTypes) {
            self::_loadMimeTypes();
        }
        $mimeType = strtolower($mimeType);
        if(isset(self::$_mimeTypes[$mimeType]['extension'])) {
            return self::$_mimeTypes[$mimeType]['extension'];
        }
        return false;
    }

    public static function getLabelByExtension($extension) {
        if(!self::$_extensionTypes) {
            self::_loadExtensionTypes();
        }
        $extension = strtolower($extension);
        if(isset(self::$_mimeTypes[$extension]['label'][0])) {
            return self::$_mimeTypes[$extension]['label'][0];
        }
        return false;
    }

    public static function getMimeTypeByExtension($extension) {
        if(!self::$_extensionTypes) {
            self::_loadExtensionTypes();
        }
        $extension = strtolower($extension);
        if(isset(self::$_mimeTypes[$extension]['mimeTypes'][0])) {
            return self::$_mimeTypes[$extension]['mimeTypes'][0];
        }
        return false;
    }

    public static function isBinary($sourcePath) {
        $type = strtolower(self::getType($sourcePath));
        return self::isTypeBinary($type);
    }

    public static function isTypeBinary($type) {
        if(strpos($type, 'text/') === 0) {
            return false; // text type
        }
        if(in_array($type, self::$_textExceptions)) {
            return false;
        }
        return true; // other (most are binary - add text exceptions as needed
    }
}