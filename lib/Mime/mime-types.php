<?php
return array(
    'text/h323' => array(
        'extension' => '.323',
        'label' => 'Internet Telephony',
    ),
    'audio/aac' => array(
        'extension' => '.aac',
        'label' => 'AAC Audio File',
    ),
    'application/abiword' => array(
        'extension' => '.abw',
        'label' => 'AbiWord Document',
    ),
    'application/internet-property-stream' => array(
        'extension' => '.acx',
        'label' => 'Atari ST Executable',
    ),
    'application/illustrator' => array(
        'extension' => '.ai',
        'label' => 'Adobe Illustrator File',
    ),
    'audio/aiff' => array(
        'extension' => '.aif',
        'label' => 'AIFF Audio File',
    ),
    'audio/aifc' => array(
        'extension' => '.aif',
        'label' => 'AIFF Audio File',
    ),
    'audio/x-aiff' => array(
        'extension' => '.aif',
        'label' => 'AIFF Audio File',
    ),
    'audio/aiff' => array(
        'extension' => '.aifc',
        'label' => 'AIFF Audio File',
    ),
    'audio/aifc' => array(
        'extension' => '.aifc',
        'label' => 'AIFF Audio File',
    ),
    'audio/x-aiff' => array(
        'extension' => '.aifc',
        'label' => 'AIFF Audio File',
    ),
    'audio/aiff' => array(
        'extension' => '.aiff',
        'label' => 'AIFF Audio File',
    ),
    'audio/aifc' => array(
        'extension' => '.aiff',
        'label' => 'AIFF Audio File',
    ),
    'audio/x-aiff' => array(
        'extension' => '.aiff',
        'label' => 'AIFF Audio File',
    ),
    'video/x-ms-asf' => array(
        'extension' => '.asf',
        'label' => 'Windows Media File',
    ),
    'application/x-asp' => array(
        'extension' => '.asp',
        'label' => 'ASP Source File',
    ),
    'text/asp' => array(
        'extension' => '.asp',
        'label' => 'ASP Source File',
    ),
    'video/x-ms-asf' => array(
        'extension' => '.asr',
        'label' => 'Windows Media File',
    ),
    'video/x-ms-asf' => array(
        'extension' => '.asx',
        'label' => 'Windows Media File',
    ),
    'audio/basic' => array(
        'extension' => '.au',
        'label' => 'Audio File',
    ),
    'audio/au' => array(
        'extension' => '.au',
        'label' => 'Audio File',
    ),
    'audio/x-au' => array(
        'extension' => '.au',
        'label' => 'Audio File',
    ),
    'audio/x-basic' => array(
        'extension' => '.au',
        'label' => 'Audio File',
    ),
    'video/avi' => array(
        'extension' => '.avi',
        'label' => 'AVI Video File',
    ),
    'application/x-troff-msvideo' => array(
        'extension' => '.avi',
        'label' => 'AVI Video File',
    ),
    'image/avi' => array(
        'extension' => '.avi',
        'label' => 'AVI Video File',
    ),
    'video/msvideo' => array(
        'extension' => '.avi',
        'label' => 'AVI Video File',
    ),
    'video/x-msvideo' => array(
        'extension' => '.avi',
        'label' => 'AVI Video File',
    ),
    'video/xmpg2' => array(
        'extension' => '.avi',
        'label' => 'AVI Video File',
    ),
    'application/olescript' => array(
        'extension' => '.axs',
        'label' => 'ActiveX Script',
    ),
    'text/plain' => array(
        'extension' => '.bas',
        'label' => 'BASIC Source Code',
    ),
    'application/octet-stream' => array(
        'extension' => '.bin',
        'label' => 'Binary File',
    ),
    'application/bin' => array(
        'extension' => '.bin',
        'label' => 'Binary File',
    ),
    'application/binary' => array(
        'extension' => '.bin',
        'label' => 'Binary File',
    ),
    'application/x-msdownload' => array(
        'extension' => '.bin',
        'label' => 'Binary File',
    ),
    'image/bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'application/bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'application/x-bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/ms-bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/x-bitmap' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/x-bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/x-ms-bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/x-win-bitmap' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/x-windows-bmp' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'image/x-xbitmap' => array(
        'extension' => '.bmp',
        'label' => 'Bitmap Image',
    ),
    'application/x-bzip2' => array(
        'extension' => '.bz2',
        'label' => 'Compressed Bzip2 File',
    ),
    'application/bzip2' => array(
        'extension' => '.bz2',
        'label' => 'Compressed Bzip2 File',
    ),
    'application/x-bz2' => array(
        'extension' => '.bz2',
        'label' => 'Compressed Bzip2 File',
    ),
    'application/x-bzip' => array(
        'extension' => '.bz2',
        'label' => 'Compressed Bzip2 File',
    ),
    'text/x-csrc' => array(
        'extension' => '.c',
        'label' => 'C Source File',
    ),
    'text/x-c++src' => array(
        'extension' => '.c++',
        'label' => 'C++ Source File',
    ),
    'application/vnd.ms-cab-compressed' => array(
        'extension' => '.cab',
        'label' => 'Microsoft Cabinet Archive',
    ),
    'application/cab' => array(
        'extension' => '.cab',
        'label' => 'Microsoft Cabinet Archive',
    ),
    'application/x-cabinet' => array(
        'extension' => '.cab',
        'label' => 'Microsoft Cabinet Archive',
    ),
    'application/vnd.ms-pkiseccat' => array(
        'extension' => '.cat',
        'label' => 'Security Catalog',
    ),
    'application/x-director' => array(
        'extension' => '.cct',
        'label' => 'Adobe Director File',
    ),
    'application/cdf' => array(
        'extension' => '.cdf',
        'label' => 'Channel Definition Format',
    ),
    'application/x-cdf' => array(
        'extension' => '.cdf',
        'label' => 'Channel Definition Format',
    ),
    'application/netcdf' => array(
        'extension' => '.cdf',
        'label' => 'Channel Definition Format',
    ),
    'application/x-netcdf' => array(
        'extension' => '.cdf',
        'label' => 'Channel Definition Format',
    ),
    'text/cdf' => array(
        'extension' => '.cdf',
        'label' => 'Channel Definition Format',
    ),
    'text/x-cdf' => array(
        'extension' => '.cdf',
        'label' => 'Channel Definition Format',
    ),
    'application/x-x509-ca-cert' => array(
        'extension' => '.cer',
        'label' => 'Internet Security Certificate File',
    ),
    'application/pkix-cert' => array(
        'extension' => '.cer',
        'label' => 'Internet Security Certificate File',
    ),
    'application/x-pkcs12' => array(
        'extension' => '.cer',
        'label' => 'Internet Security Certificate File',
    ),
    'application/keychain_access' => array(
        'extension' => '.cer',
        'label' => 'Internet Security Certificate File',
    ),
    'application/x-cfm' => array(
        'extension' => '.cfc',
        'label' => 'ColdFusion Source File',
    ),
    'application/x-cfm' => array(
        'extension' => '.cfm',
        'label' => 'ColdFusion Source File',
    ),
    'application/x-java' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/java' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/java-byte-code' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/java-vm' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-java-applet' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-java-bean' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-java-class' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-java-vm' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-jinit-bean' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-jinit-applet' => array(
        'extension' => '.class',
        'label' => 'Java Bytecode File',
    ),
    'application/x-msclip' => array(
        'extension' => '.clp',
        'label' => 'Windows Clipboard/Picture',
    ),
    'image/x-cmx' => array(
        'extension' => '.cmx',
        'label' => 'Presentation Exchange Image',
    ),
    'application/cmx' => array(
        'extension' => '.cmx',
        'label' => 'Presentation Exchange Image',
    ),
    'application/x-cmx' => array(
        'extension' => '.cmx',
        'label' => 'Presentation Exchange Image',
    ),
    'drawing/cmx' => array(
        'extension' => '.cmx',
        'label' => 'Presentation Exchange Image',
    ),
    'image/x-cmx' => array(
        'extension' => '.cmx',
        'label' => 'Presentation Exchange Image',
    ),
    'image/cis-cod' => array(
        'extension' => '.cod',
        'label' => 'CIS-COD File',
    ),
    'text/x-c++src' => array(
        'extension' => '.cp',
        'label' => 'C++ Source File',
    ),
    'application/x-cpio' => array(
        'extension' => '.cpio',
        'label' => 'UNIX CPIO Archive',
    ),
    'text/x-c++src' => array(
        'extension' => '.cpp',
        'label' => 'C++ Source File',
    ),
    'application/x-mscardfile' => array(
        'extension' => '.crd',
        'label' => 'Windows Cardfile',
    ),
    'application/x-x509-ca-cert' => array(
        'extension' => '.crt',
        'label' => 'Internet Security Certificate File',
    ),
    'application/pkix-cert' => array(
        'extension' => '.crt',
        'label' => 'Internet Security Certificate File',
    ),
    'application/x-pkcs12' => array(
        'extension' => '.crt',
        'label' => 'Internet Security Certificate File',
    ),
    'application/keychain_access' => array(
        'extension' => '.crt',
        'label' => 'Internet Security Certificate File',
    ),
    'application/pkix-crl' => array(
        'extension' => '.crl',
        'label' => 'Certificate Revocation List',
    ),
    'application/x-x509-ca-cert' => array(
        'extension' => '.crt',
        'label' => 'Certificate File',
    ),
    'application/pkix-cert' => array(
        'extension' => '.crt',
        'label' => 'Certificate File',
    ),
    'application/keychain_access' => array(
        'extension' => '.crt',
        'label' => 'Certificate File',
    ),
    'application/x-csh' => array(
        'extension' => '.csh',
        'label' => 'C Shell File',
    ),
    'text/css' => array(
        'extension' => '.css',
        'label' => 'Cascading Stylesheet File',
    ),
    'application/css-stylesheet' => array(
        'extension' => '.css',
        'label' => 'Cascading Stylesheet File',
    ),
    'application/x-director' => array(
        'extension' => '.cst',
        'label' => 'Adobe Director File',
    ),
    'text/csv' => array(
        'extension' => '.csv',
        'label' => 'Comma-delimited File',
    ),
    'application/csv' => array(
        'extension' => '.csv',
        'label' => 'Comma-delimited File',
    ),
    'text/comma-separated-values' => array(
        'extension' => '.csv',
        'label' => 'Comma-delimited File',
    ),
    'text/x-comma-separated-values' => array(
        'extension' => '.csv',
        'label' => 'Comma-delimited File',
    ),
    'application/x-director' => array(
        'extension' => '.cxt',
        'label' => 'Adobe Director File',
    ),
    'application/x-director' => array(
        'extension' => '.dcr',
        'label' => 'Adobe Director File',
    ),
    'application/x-x509-ca-cert' => array(
        'extension' => '.der',
        'label' => 'Internet Security Certificate File',
    ),
    'application/pkix-cert' => array(
        'extension' => '.der',
        'label' => 'Internet Security Certificate File',
    ),
    'application/x-pkcs12' => array(
        'extension' => '.der',
        'label' => 'Internet Security Certificate File',
    ),
    'application/keychain_access' => array(
        'extension' => '.der',
        'label' => 'Internet Security Certificate File',
    ),
    'image/bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'application/bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'application/x-bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/ms-bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/x-bitmap' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/x-bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/x-ms-bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/x-win-bitmap' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/x-windows-bmp' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'image/x-xbitmap' => array(
        'extension' => '.dib',
        'label' => 'Bitmap Image',
    ),
    'text/x-patch' => array(
        'extension' => '.diff',
        'label' => 'Patch Source File',
    ),
    'application/x-director' => array(
        'extension' => '.dir',
        'label' => 'Adobe Director File',
    ),
    'application/x-msdownload' => array(
        'extension' => '.dll',
        'label' => 'Dynamic Link Library',
    ),
    'application/octet-stream' => array(
        'extension' => '.dll',
        'label' => 'Dynamic Link Library',
    ),
    'application/x-msdos-program' => array(
        'extension' => '.dll',
        'label' => 'Dynamic Link Library',
    ),
    'application/octet-stream' => array(
        'extension' => '.dms',
        'label' => 'DISKMASHER Compressed Archive',
    ),
    'application/vnd.ms-word' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/doc' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/msword' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/msword-doc' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/vnd.msword' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/winword' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/word' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/x-msw6' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/x-msword' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/x-msword-doc' => array(
        'extension' => '.doc',
        'label' => 'Microsoft Word Document',
    ),
    'application/vnd.ms-word.document.macroEnabled.12' => array(
        'extension' => '.docm',
        'label' => 'Microsoft Word Document',
    ),
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => array(
        'extension' => '.docx',
        'label' => 'Microsoft Word File',
    ),
    'application/vnd.ms-word.document.12' => array(
        'extension' => '.docx',
        'label' => 'Microsoft Word File',
    ),
    'application/vnd.openxmlformats-officedocument.word' => array(
        'extension' => '.docx',
        'label' => 'Microsoft Word File',
    ),
    'application/msword' => array(
        'extension' => '.dot',
        'label' => 'Word Document Template',
    ),
    'application/vnd.ms-word.template.macroEnabled.12' => array(
        'extension' => '.dotm',
        'label' => 'Microsoft Word Template File',
    ),
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template' => array(
        'extension' => '.dotx',
        'label' => 'Microsoft Word Template File',
    ),
    'application/x-stata' => array(
        'extension' => '.dta',
        'label' => 'Stata Data File',
    ),
    'video/x-dv' => array(
        'extension' => '.dv',
        'label' => 'Digital Video File',
    ),
    'application/x-dvi' => array(
        'extension' => '.dvi',
        'label' => 'DVI File',
    ),
    'image/x-dwg' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/acad' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/autocad_dwg' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/dwg' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/x-acad' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/x-autocad' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/x-dwg' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'image/vnd.dwg' => array(
        'extension' => '.dwg',
        'label' => 'AutoCAD Drawing',
    ),
    'application/x-autocad' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'application/dxf' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'application/x-dxf' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'drawing/x-dxf' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'image/vnd.dxf' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'image/x-autocad' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'image/x-dxf' => array(
        'extension' => '.dxf',
        'label' => 'AutoCAD Drawing',
    ),
    'application/x-director' => array(
        'extension' => '.dxr',
        'label' => 'Adobe Director File',
    ),
    'application/x-elc' => array(
        'extension' => '.elc',
        'label' => 'Emacs Source File',
    ),
    'message/rfc822' => array(
        'extension' => '.eml',
        'label' => 'Web Archive File',
    ),
    'application/x-endnote-library' => array(
        'extension' => '.enl',
        'label' => 'EndNote Library File',
    ),
    'application/x-endnote-refer' => array(
        'extension' => '.enl',
        'label' => 'EndNote Library File',
    ),
    'application/x-endnote-library' => array(
        'extension' => '.enz',
        'label' => 'EndNote Library File',
    ),
    'application/x-endnote-refer' => array(
        'extension' => '.enz',
        'label' => 'EndNote Library File',
    ),
    'application/postscript' => array(
        'extension' => '.eps',
        'label' => 'PostScript File',
    ),
    'application/eps' => array(
        'extension' => '.eps',
        'label' => 'PostScript File',
    ),
    'application/x-eps' => array(
        'extension' => '.eps',
        'label' => 'PostScript File',
    ),
    'image/eps' => array(
        'extension' => '.eps',
        'label' => 'PostScript File',
    ),
    'image/x-eps' => array(
        'extension' => '.eps',
        'label' => 'PostScript File',
    ),
    'text/x-setext' => array(
        'extension' => '.etx',
        'label' => 'Setext (Structure Enhanced Text)',
    ),
    'text/anytext' => array(
        'extension' => '.etx',
        'label' => 'Setext (Structure Enhanced Text)',
    ),
    'application/envoy' => array(
        'extension' => '.evy',
        'label' => 'Envoy Document',
    ),
    'application/x-envoy' => array(
        'extension' => '.evy',
        'label' => 'Envoy Document',
    ),
    'application/x-msdos-program' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/dos-exe' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/exe' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/msdos-windows' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/x-sdlc' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/x-exe' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/x-winexe' => array(
        'extension' => '.exe',
        'label' => 'Windows Executable File',
    ),
    'application/fractals' => array(
        'extension' => '.fif',
        'label' => 'Fractal Image Format',
    ),
    'image/fif' => array(
        'extension' => '.fif',
        'label' => 'Fractal Image Format',
    ),
    'x-world/x-vrml' => array(
        'extension' => '.flr',
        'label' => 'Virtual Reality Modeling Language File',
    ),
    'application/vnd.framemaker' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/framemaker' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/maker' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/vnd.mif' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/x-framemaker' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/x-maker' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/x-mif' => array(
        'extension' => '.fm',
        'label' => 'FrameMaker File',
    ),
    'application/x-director' => array(
        'extension' => '.fqd',
        'label' => 'Adobe Director File',
    ),
    'image/gif' => array(
        'extension' => '.gif',
        'label' => 'GIF Image',
    ),
    'application/tar' => array(
        'extension' => '.gtar',
        'label' => 'Compressed Tar File',
    ),
    'application/x-gtar' => array(
        'extension' => '.gtar',
        'label' => 'Compressed Tar File',
    ),
    'application/x-tar' => array(
        'extension' => '.gtar',
        'label' => 'Compressed Tar File',
    ),
    'application/gzip' => array(
        'extension' => '.gz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/gzip-compressed' => array(
        'extension' => '.gz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/gzipped' => array(
        'extension' => '.gz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/x-gunzip' => array(
        'extension' => '.gz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/x-gzip' => array(
        'extension' => '.gz',
        'label' => 'Compressed Gzip Archive',
    ),
    'text/x-chdr' => array(
        'extension' => '.h',
        'label' => 'C Header File',
    ),
    'application/x-hdf' => array(
        'extension' => '.hdf',
        'label' => 'Hierarchical Data Format File',
    ),
    'application/winhlp' => array(
        'extension' => '.hlp',
        'label' => 'Windows Help File',
    ),
    'application/x-helpfile' => array(
        'extension' => '.hlp',
        'label' => 'Windows Help File',
    ),
    'application/x-winhelp' => array(
        'extension' => '.hlp',
        'label' => 'Windows Help File',
    ),
    'application/binhex' => array(
        'extension' => '.hqx',
        'label' => 'BinHex Archive',
    ),
    'application/mac-binhex' => array(
        'extension' => '.hqx',
        'label' => 'BinHex Archive',
    ),
    'application/mac-binhex40' => array(
        'extension' => '.hqx',
        'label' => 'BinHex Archive',
    ),
    'application/hta' => array(
        'extension' => '.hta',
        'label' => 'HTML Application File',
    ),
    'text/x-component' => array(
        'extension' => '.htc',
        'label' => 'HTML Component File',
    ),
    'text/html' => array(
        'extension' => '.htm',
        'label' => 'HTML File',
    ),
    'application/xhtml+xml' => array(
        'extension' => '.htm',
        'label' => 'HTML File',
    ),
    'text/html' => array(
        'extension' => '.html',
        'label' => 'HTML File',
    ),
    'application/xhtml+xml' => array(
        'extension' => '.html',
        'label' => 'HTML File',
    ),
    'text/webviewhtml' => array(
        'extension' => '.htt',
        'label' => 'Hypertext Template File',
    ),
    'image/x-ico' => array(
        'extension' => '.ico',
        'label' => 'Favicon, Icon File',
    ),
    'text/calendar' => array(
        'extension' => '.ics',
        'label' => 'Calendar File',
    ),
    'image/ief' => array(
        'extension' => '.ief',
        'label' => 'IEF Image File',
    ),
    'application/x-iphone' => array(
        'extension' => '.iii',
        'label' => 'Intel IPhone Compatible File',
    ),
    'application/x-indesign' => array(
        'extension' => '.indd',
        'label' => 'Adobe InDesign File',
    ),
    'application/x-internet-signup' => array(
        'extension' => '.ins',
        'label' => 'IIS Internet Communications Settings File',
    ),
    'application/x-internet-signup' => array(
        'extension' => '.isp',
        'label' => 'IIS Internet Service Provider Settings File',
    ),
    'text/vnd.sun.j2me.app-descriptor' => array(
        'extension' => '.jad',
        'label' => 'Java Application Descriptor',
    ),
    'application/java-archive' => array(
        'extension' => '.jar',
        'label' => 'Java Archive File',
    ),
    'text/x-java' => array(
        'extension' => '.java',
        'label' => 'Java Source File',
    ),
    'java/*' => array(
        'extension' => '.java',
        'label' => 'Java Source File',
    ),
    'text/java' => array(
        'extension' => '.java',
        'label' => 'Java Source File',
    ),
    'text/x-java-source' => array(
        'extension' => '.java',
        'label' => 'Java Source File',
    ),
    'image/jpeg' => array(
        'extension' => '.jfif',
        'label' => 'JPEG Image',
    ),
    'image/pjpeg' => array(
        'extension' => '.jfif',
        'label' => 'JPEG Image',
    ),
    'image/jpeg' => array(
        'extension' => '.jpe',
        'label' => 'JPEG Image',
    ),
    'image/pjpeg' => array(
        'extension' => '.jpe',
        'label' => 'JPEG Image',
    ),
    'image/jpeg' => array(
        'extension' => '.jpeg',
        'label' => 'JPEG Image',
    ),
    'image/pjpeg' => array(
        'extension' => '.jpeg',
        'label' => 'JPEG Image',
    ),
    'image/jpeg' => array(
        'extension' => '.jpg',
        'label' => 'JPEG Image',
    ),
    'image/pjpeg' => array(
        'extension' => '.jpg',
        'label' => 'JPEG Image',
    ),
    'text/javascript' => array(
        'extension' => '.js',
        'label' => 'JavaScript Source File',
    ),
    'application/javascript' => array(
        'extension' => '.js',
        'label' => 'JavaScript Source File',
    ),
    'application/x-javascript' => array(
        'extension' => '.js',
        'label' => 'JavaScript Source File',
    ),
    'application/x-js' => array(
        'extension' => '.js',
        'label' => 'JavaScript Source File',
    ),
    'application/vnd.google-earth.kml+xml' => array(
        'extension' => '.kml',
        'label' => 'KML File',
    ),
    'application/vnd.google-earth.kmz' => array(
        'extension' => '.kmz',
        'label' => 'Compressed KML File',
    ),
    'application/x-latex' => array(
        'extension' => '.latex',
        'label' => 'LATEX File',
    ),
    'text/x-latex' => array(
        'extension' => '.latex',
        'label' => 'LATEX File',
    ),
    'application/x-lha' => array(
        'extension' => '.lha',
        'label' => 'Compressed Archive File',
    ),
    'application/lha' => array(
        'extension' => '.lha',
        'label' => 'Compressed Archive File',
    ),
    'application/lzh' => array(
        'extension' => '.lha',
        'label' => 'Compressed Archive File',
    ),
    'application/x-lzh' => array(
        'extension' => '.lha',
        'label' => 'Compressed Archive File',
    ),
    'application/x-lzh-archive' => array(
        'extension' => '.lha',
        'label' => 'Compressed Archive File',
    ),
    'application/x-endnote-library' => array(
        'extension' => '.lib',
        'label' => 'EndNote Library File',
    ),
    'application/x-endnote-refer' => array(
        'extension' => '.lib',
        'label' => 'EndNote Library File',
    ),
    'application/x-labview' => array(
        'extension' => '.llb',
        'label' => 'LabVIEW Application File',
    ),
    'application/x-labview-vi' => array(
        'extension' => '.llb',
        'label' => 'LabVIEW Application File',
    ),
    'text/x-log' => array(
        'extension' => '.log',
        'label' => 'Log Text File',
    ),
    'video/x-la-asf' => array(
        'extension' => '.lsf',
        'label' => 'Streaming Audio/Video File',
    ),
    'video/x-la-asf' => array(
        'extension' => '.lsx',
        'label' => 'Streaming Audio/Video File',
    ),
    'application/x-labview-exec' => array(
        'extension' => '.lvx',
        'label' => 'LabVIEW CAL Simulation File',
    ),
    'application/x-lha' => array(
        'extension' => '.lzh',
        'label' => 'Compressed Archive File',
    ),
    'application/lha' => array(
        'extension' => '.lzh',
        'label' => 'Compressed Archive File',
    ),
    'application/lzh' => array(
        'extension' => '.lzh',
        'label' => 'Compressed Archive File',
    ),
    'application/x-lzh' => array(
        'extension' => '.lzh',
        'label' => 'Compressed Archive File',
    ),
    'application/x-lzh-archive' => array(
        'extension' => '.lzh',
        'label' => 'Compressed Archive File',
    ),
    'text/x-objcsrc' => array(
        'extension' => '.m',
        'label' => 'Objective C Source File',
    ),
    'video/mpeg' => array(
        'extension' => '.m1v',
        'label' => 'MPEG Video File',
    ),
    'video/mpeg' => array(
        'extension' => '.m2v',
        'label' => 'MPEG Video File',
    ),
    'audio/x-mpegurl' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'application/x-winamp-playlist' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'audio/mpegurl' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'audio/mpeg-url' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'audio/playlist' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'audio/scpls' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'audio/x-scpls' => array(
        'extension' => '.m3u',
        'label' => 'MP3 Playlist File',
    ),
    'audio/m4a' => array(
        'extension' => '.m4a',
        'label' => 'MPEG-4 Audio File',
    ),
    'audio/x-m4a' => array(
        'extension' => '.m4a',
        'label' => 'MPEG-4 Audio File',
    ),
    'video/mp4' => array(
        'extension' => '.m4v',
        'label' => 'MPEG-4 Video File',
    ),
    'video/mpeg4' => array(
        'extension' => '.m4v',
        'label' => 'MPEG-4 Video File',
    ),
    'video/x-m4v' => array(
        'extension' => '.m4v',
        'label' => 'MPEG-4 Video File',
    ),
    'application/mathematica' => array(
        'extension' => '.ma',
        'label' => 'Mathematica File',
    ),
    'message/rfc822' => array(
        'extension' => '.mail',
        'label' => 'Web Archive File',
    ),
    'application/x-troff-man' => array(
        'extension' => '.man',
        'label' => 'Troff With MAN Macros File',
    ),
    'application/x-mathcad' => array(
        'extension' => '.mcd',
        'label' => 'MathCaD File',
    ),
    'application/mcad' => array(
        'extension' => '.mcd',
        'label' => 'MathCaD File',
    ),
    'application/vnd.ms-access' => array(
        'extension' => '.mdb',
        'label' => 'Microsoft Access File',
    ),
    'application/mdb' => array(
        'extension' => '.mdb',
        'label' => 'Microsoft Access File',
    ),
    'application/msaccess' => array(
        'extension' => '.mdb',
        'label' => 'Microsoft Access File',
    ),
    'application/vnd.msaccess' => array(
        'extension' => '.mdb',
        'label' => 'Microsoft Access File',
    ),
    'application/x-mdb' => array(
        'extension' => '.mdb',
        'label' => 'Microsoft Access File',
    ),
    'application/x-msaccess' => array(
        'extension' => '.mdb',
        'label' => 'Microsoft Access File',
    ),
    'application/x-troff-me' => array(
        'extension' => '.me',
        'label' => 'Troff With ME Macros File',
    ),
    'application/x-shockwave-flash' => array(
        'extension' => '.mfp',
        'label' => 'Adobe Flash File',
    ),
    'application/futuresplash' => array(
        'extension' => '.mfp',
        'label' => 'Adobe Flash File',
    ),
    'message/rfc822' => array(
        'extension' => '.mht',
        'label' => 'Web Archive File',
    ),
    'message/rfc822' => array(
        'extension' => '.mhtml',
        'label' => 'Web Archive File',
    ),
    'audio/x-midi' => array(
        'extension' => '.mid',
        'label' => 'MIDI Audio File',
    ),
    'application/x-midi' => array(
        'extension' => '.mid',
        'label' => 'MIDI Audio File',
    ),
    'audio/mid' => array(
        'extension' => '.mid',
        'label' => 'MIDI Audio File',
    ),
    'audio/midi' => array(
        'extension' => '.mid',
        'label' => 'MIDI Audio File',
    ),
    'audio/soundtrack' => array(
        'extension' => '.mid',
        'label' => 'MIDI Audio File',
    ),
    'audio/x-midi' => array(
        'extension' => '.midi',
        'label' => 'MIDI Audio File',
    ),
    'application/x-midi' => array(
        'extension' => '.midi',
        'label' => 'MIDI Audio File',
    ),
    'audio/mid' => array(
        'extension' => '.midi',
        'label' => 'MIDI Audio File',
    ),
    'audio/midi' => array(
        'extension' => '.midi',
        'label' => 'MIDI Audio File',
    ),
    'audio/soundtrack' => array(
        'extension' => '.midi',
        'label' => 'MIDI Audio File',
    ),
    'application/vnd.framemaker' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/framemaker' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/maker' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/vnd.mif' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/x-framemaker' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/x-maker' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/x-mif' => array(
        'extension' => '.mif',
        'label' => 'FrameMaker File',
    ),
    'application/x-msmoney' => array(
        'extension' => '.mny',
        'label' => 'Money Data File',
    ),
    'video/quicktime' => array(
        'extension' => '.mov',
        'label' => 'Quicktime Video File',
    ),
    'video/mpeg' => array(
        'extension' => '.mp2',
        'label' => 'MPEG Layer 2 Audio File',
    ),
    'audio/mpeg' => array(
        'extension' => '.mp2',
        'label' => 'MPEG Layer 2 Audio File',
    ),
    'audio/x-mpeg' => array(
        'extension' => '.mp2',
        'label' => 'MPEG Layer 2 Audio File',
    ),
    'audio/x-mpeg-2' => array(
        'extension' => '.mp2',
        'label' => 'MPEG Layer 2 Audio File',
    ),
    'video/x-mpeg' => array(
        'extension' => '.mp2',
        'label' => 'MPEG Layer 2 Audio File',
    ),
    'video/x-mpeq2a' => array(
        'extension' => '.mp2',
        'label' => 'MPEG Layer 2 Audio File',
    ),
    'audio/mpeg' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/mp3' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/mpeg3' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/mpg' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/x-mp3' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/x-mpeg' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/x-mpeg3' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'audio/x-mpg' => array(
        'extension' => '.mp3',
        'label' => 'MP3 Audio File',
    ),
    'video/mpeg' => array(
        'extension' => '.mpa',
        'label' => 'MPEG Audio Stream',
    ),
    'video/mpeg' => array(
        'extension' => '.mpe',
        'label' => 'MPEG Video File',
    ),
    'video/mpeg' => array(
        'extension' => '.mpeg',
        'label' => 'MPEG Video File',
    ),
    'video/mpeg' => array(
        'extension' => '.mpg',
        'label' => 'MPEG Video File',
    ),
    'application/vnd.ms-project' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'application/mpp' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'application/msproj' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'application/msproject' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'application/x-dos_ms_project' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'application/x-ms-project' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'application/x-msproject' => array(
        'extension' => '.mpp',
        'label' => 'Microsoft Project File',
    ),
    'video/mpeg' => array(
        'extension' => '.mpv2',
        'label' => 'MPEG Audio Stream',
    ),
    'video/quicktime' => array(
        'extension' => '.mqv',
        'label' => 'Quicktime Video File',
    ),
    'application/x-troff-ms' => array(
        'extension' => '.ms',
        'label' => 'Troff With MS Macros File',
    ),
    'application/x-msmediaview' => array(
        'extension' => '.mvb',
        'label' => 'Multimedia Viewer',
    ),
    'application/x-maple' => array(
        'extension' => '.mws',
        'label' => 'Maple Worksheet File',
    ),
    'application/maple-v-r4' => array(
        'extension' => '.mws',
        'label' => 'Maple Worksheet File',
    ),
    'application/mathematica' => array(
        'extension' => '.nb',
        'label' => 'Mathematica File',
    ),
    'message/rfc822' => array(
        'extension' => '.nws',
        'label' => 'Web Archive File',
    ),
    'application/oda' => array(
        'extension' => '.oda',
        'label' => 'ODA Document',
    ),
    'application/vnd.oasis.opendocument.formula' => array(
        'extension' => '.odf',
        'label' => 'OpenOffice Formula File',
    ),
    'application/vnd.oasis.opendocument.graphics' => array(
        'extension' => '.odg',
        'label' => 'OpenOffice Graphics File',
    ),
    'application/vnd.oasis.opendocument.presentation' => array(
        'extension' => '.odp',
        'label' => 'OpenOffice Presentation File',
    ),
    'application/vnd.oasis.opendocument.spreadsheet' => array(
        'extension' => '.ods',
        'label' => 'OpenOffice Spreadsheet File',
    ),
    'application/vnd.oasis.opendocument.text' => array(
        'extension' => '.odt',
        'label' => 'OpenOffice Document',
    ),
    'application/ogg' => array(
        'extension' => '.ogg',
        'label' => 'OGG Audio File',
    ),
    'application/x-ogg' => array(
        'extension' => '.ogg',
        'label' => 'OGG Audio File',
    ),
    'audio/x-ogg' => array(
        'extension' => '.ogg',
        'label' => 'OGG Audio File',
    ),
    'application/msonenote' => array(
        'extension' => '.one',
        'label' => 'Microsoft OneNote File',
    ),
    'application/x-x509-ca-cert' => array(
        'extension' => '.p12',
        'label' => 'Internet Security Certificate File',
    ),
    'application/pkix-cert' => array(
        'extension' => '.p12',
        'label' => 'Internet Security Certificate File',
    ),
    'application/x-pkcs12' => array(
        'extension' => '.p12',
        'label' => 'Internet Security Certificate File',
    ),
    'application/keychain_access' => array(
        'extension' => '.p12',
        'label' => 'Internet Security Certificate File',
    ),
    'text/x-patch' => array(
        'extension' => '.patch',
        'label' => 'Patch Source File',
    ),
    'image/x-portable-bitmap' => array(
        'extension' => '.pbm',
        'label' => 'Portable Bitmap Image',
    ),
    'image/pbm' => array(
        'extension' => '.pbm',
        'label' => 'Portable Bitmap Image',
    ),
    'image/portable-bitmap' => array(
        'extension' => '.pbm',
        'label' => 'Portable Bitmap Image',
    ),
    'image/x-pbm' => array(
        'extension' => '.pbm',
        'label' => 'Portable Bitmap Image',
    ),
    'image/x-photo-cd' => array(
        'extension' => '.pcd',
        'label' => 'Kodak Photo CD File',
    ),
    'image/pcd' => array(
        'extension' => '.pcd',
        'label' => 'Kodak Photo CD File',
    ),
    'image/x-pict' => array(
        'extension' => '.pct',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/pict' => array(
        'extension' => '.pct',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/x-macpict' => array(
        'extension' => '.pct',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'application/pdf' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'application/acrobat' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'application/nappdf' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'application/x-pdf' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'application/vnd.pdf' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'text/pdf' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'text/x-pdf' => array(
        'extension' => '.pdf',
        'label' => 'Adobe Acrobat File',
    ),
    'application/x-pkcs12' => array(
        'extension' => '.pfx',
        'label' => 'Personal Information Exchange File',
    ),
    'image/x-portable-graymap' => array(
        'extension' => '.pgm',
        'label' => 'Portable Graymap Image',
    ),
    'image/x-pgm' => array(
        'extension' => '.pgm',
        'label' => 'Portable Graymap Image',
    ),
    'application/x-php' => array(
        'extension' => '.php',
        'label' => 'PHP Source File',
    ),
    'application/php' => array(
        'extension' => '.php',
        'label' => 'PHP Source File',
    ),
    'text/php' => array(
        'extension' => '.php',
        'label' => 'PHP Source File',
    ),
    'text/x-php' => array(
        'extension' => '.php',
        'label' => 'PHP Source File',
    ),
    'image/x-pict' => array(
        'extension' => '.pic',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/pict' => array(
        'extension' => '.pic',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/x-macpict' => array(
        'extension' => '.pic',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/x-pict' => array(
        'extension' => '.pict',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/pict' => array(
        'extension' => '.pict',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/x-macpict' => array(
        'extension' => '.pict',
        'label' => 'Macintosh Quickdraw Image',
    ),
    'image/jpeg' => array(
        'extension' => '.pjpeg',
        'label' => 'JPEG Image',
    ),
    'image/pjpeg' => array(
        'extension' => '.pjpeg',
        'label' => 'JPEG Image',
    ),
    'application/x-perl' => array(
        'extension' => '.pl',
        'label' => 'Perl Source File',
    ),
    'text/x-perl' => array(
        'extension' => '.pl',
        'label' => 'Perl Source File',
    ),
    'audio/x-mpegurl' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'application/x-winamp-playlist' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'audio/mpegurl' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'audio/mpeg-url' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'audio/playlist' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'audio/scpls' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'audio/x-scpls' => array(
        'extension' => '.pls',
        'label' => 'MP3 Playlist File',
    ),
    'application/ynd.ms-pkipko' => array(
        'extension' => '.pko',
        'label' => 'PublicKey Security Object',
    ),
    'application/x-perl' => array(
        'extension' => '.pm',
        'label' => 'Perl Source File',
    ),
    'text/x-perl' => array(
        'extension' => '.pm',
        'label' => 'Perl Source File',
    ),
    'application/x-perfmon' => array(
        'extension' => '.pmc',
        'label' => 'Windows Performance Monitor File',
    ),
    'image/png' => array(
        'extension' => '.png',
        'label' => 'PNG Image',
    ),
    'image/x-png' => array(
        'extension' => '.png',
        'label' => 'PNG Image',
    ),
    'image/x-portable-anymap' => array(
        'extension' => '.pnm',
        'label' => 'Portable Any Map Graphic Bitmap',
    ),
    'text/x-pod' => array(
        'extension' => '.pod',
        'label' => 'Perl Documentation File',
    ),
    'application/vnd.ms-powerpoint.template.macroEnabled.12' => array(
        'extension' => '.potm',
        'label' => 'Microsoft PowerPoint Template File',
    ),
    'application/vnd.openxmlformats-officedocument.presentationml.template' => array(
        'extension' => '.potx',
        'label' => 'Microsoft PowerPoint Template File',
    ),
    'application/vnd.ms-powerpoint.addin.macroEnabled.12' => array(
        'extension' => '.ppam',
        'label' => 'Microsoft PowerPoint File',
    ),
    'image/x-portable-pixmap' => array(
        'extension' => '.ppm',
        'label' => 'Portable Pixmap Image',
    ),
    'application/ppm' => array(
        'extension' => '.ppm',
        'label' => 'Portable Pixmap Image',
    ),
    'application/x-ppm' => array(
        'extension' => '.ppm',
        'label' => 'Portable Pixmap Image',
    ),
    'image/x-ppm' => array(
        'extension' => '.ppm',
        'label' => 'Portable Pixmap Image',
    ),
    'application/vnd.ms-powerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/ms-powerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/mspowerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/powerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/ppt' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd-mspowerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd_ms-powerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/x-mspowerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/x-powerpoint' => array(
        'extension' => '.pps',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12' => array(
        'extension' => '.ppsm',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => array(
        'extension' => '.ppsx',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd.ms-powerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/ms-powerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/mspowerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/powerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/ppt' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd-mspowerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd_ms-powerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/x-mspowerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/x-powerpoint' => array(
        'extension' => '.ppt',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12' => array(
        'extension' => '.pptm',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => array(
        'extension' => '.pptx',
        'label' => 'Microsoft PowerPoint File',
    ),
    'application/pics-rules' => array(
        'extension' => '.prf',
        'label' => 'PICS Rules File',
    ),
    'application/postscript' => array(
        'extension' => '.ps',
        'label' => 'PostScript File',
    ),
    'application/eps' => array(
        'extension' => '.ps',
        'label' => 'PostScript File',
    ),
    'application/x-eps' => array(
        'extension' => '.ps',
        'label' => 'PostScript File',
    ),
    'image/eps' => array(
        'extension' => '.ps',
        'label' => 'PostScript File',
    ),
    'image/x-eps' => array(
        'extension' => '.ps',
        'label' => 'PostScript File',
    ),
    'application/photoshop' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'application/psd' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'application/x-photoshop' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'image/photoshop' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'image/psd' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'image/x-photoshop' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'image/x-psd' => array(
        'extension' => '.psd',
        'label' => 'Adobe Photoshop File',
    ),
    'application/vnd.ms-publisher' => array(
        'extension' => '.pub',
        'label' => 'Microsoft Publisher File',
    ),
    'application/x-mspublisher' => array(
        'extension' => '.pub',
        'label' => 'Microsoft Publisher File',
    ),
    'text/x-python' => array(
        'extension' => '.py',
        'label' => 'Python Source File',
    ),
    'video/quicktime' => array(
        'extension' => '.qt',
        'label' => 'Quicktime Video File',
    ),
    'audio/vnd.rn-realaudio' => array(
        'extension' => '.ra',
        'label' => 'RealAudio File',
    ),
    'audio/vnd.pn-realaudio' => array(
        'extension' => '.ra',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realaudio' => array(
        'extension' => '.ra',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realaudio-plugin' => array(
        'extension' => '.ra',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realvideo' => array(
        'extension' => '.ra',
        'label' => 'RealAudio File',
    ),
    'audio/x-realaudio' => array(
        'extension' => '.ra',
        'label' => 'RealAudio File',
    ),
    'audio/vnd.rn-realaudio' => array(
        'extension' => '.ram',
        'label' => 'RealAudio File',
    ),
    'audio/vnd.pn-realaudio' => array(
        'extension' => '.ram',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realaudio' => array(
        'extension' => '.ram',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realaudio-plugin' => array(
        'extension' => '.ram',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realvideo' => array(
        'extension' => '.ram',
        'label' => 'RealAudio File',
    ),
    'audio/x-realaudio' => array(
        'extension' => '.ram',
        'label' => 'RealAudio File',
    ),
    'application/rar' => array(
        'extension' => '.rar',
        'label' => 'Compressed Archive',
    ),
    'application/x-rar-compressed' => array(
        'extension' => '.rar',
        'label' => 'Compressed Archive',
    ),
    'image/x-cmu-raster' => array(
        'extension' => '.ras',
        'label' => 'Raster Image',
    ),
    'image/x-rgb' => array(
        'extension' => '.rgb',
        'label' => 'IRIS Image',
    ),
    'image/rgb' => array(
        'extension' => '.rgb',
        'label' => 'IRIS Image',
    ),
    'application/vnd.rn-realmedia' => array(
        'extension' => '.rm',
        'label' => 'RealMedia File',
    ),
    'audio/mid' => array(
        'extension' => '.rmi',
        'label' => 'Radio MIDI File',
    ),
    'application/x-troff' => array(
        'extension' => '.roff',
        'label' => 'Troff File',
    ),
    'audio/vnd.rn-realaudio' => array(
        'extension' => '.rpm',
        'label' => 'RealAudio File',
    ),
    'audio/vnd.pn-realaudio' => array(
        'extension' => '.rpm',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realaudio' => array(
        'extension' => '.rpm',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realaudio-plugin' => array(
        'extension' => '.rpm',
        'label' => 'RealAudio File',
    ),
    'audio/x-pn-realvideo' => array(
        'extension' => '.rpm',
        'label' => 'RealAudio File',
    ),
    'audio/x-realaudio' => array(
        'extension' => '.rpm',
        'label' => 'RealAudio File',
    ),
    'application/rtf' => array(
        'extension' => '.rtf',
        'label' => 'Rich Text Format File',
    ),
    'application/richtext' => array(
        'extension' => '.rtf',
        'label' => 'Rich Text Format File',
    ),
    'application/x-rtf' => array(
        'extension' => '.rtf',
        'label' => 'Rich Text Format File',
    ),
    'text/richtext' => array(
        'extension' => '.rtf',
        'label' => 'Rich Text Format File',
    ),
    'text/rtf' => array(
        'extension' => '.rtf',
        'label' => 'Rich Text Format File',
    ),
    'application/rtf' => array(
        'extension' => '.rtx',
        'label' => 'Rich Text Format File',
    ),
    'application/richtext' => array(
        'extension' => '.rtx',
        'label' => 'Rich Text Format File',
    ),
    'application/x-rtf' => array(
        'extension' => '.rtx',
        'label' => 'Rich Text Format File',
    ),
    'text/richtext' => array(
        'extension' => '.rtx',
        'label' => 'Rich Text Format File',
    ),
    'text/rtf' => array(
        'extension' => '.rtx',
        'label' => 'Rich Text Format File',
    ),
    'video/vnd.rn-realvideo' => array(
        'extension' => '.rv',
        'label' => 'RealVideo File',
    ),
    'video/x-pn-realvideo' => array(
        'extension' => '.rv',
        'label' => 'RealVideo File',
    ),
    'application/sas' => array(
        'extension' => '.sas',
        'label' => 'SAS File',
    ),
    'application/x-sas' => array(
        'extension' => '.sas',
        'label' => 'SAS File',
    ),
    'application/x-sas-data' => array(
        'extension' => '.sas',
        'label' => 'SAS File',
    ),
    'application/x-sas-log' => array(
        'extension' => '.sas',
        'label' => 'SAS File',
    ),
    'application/x-sas-output' => array(
        'extension' => '.sas',
        'label' => 'SAS File',
    ),
    'application/spss' => array(
        'extension' => '.sav',
        'label' => 'SPSS File',
    ),
    'application/x-msschedule' => array(
        'extension' => '.scd',
        'label' => 'Schedule Data',
    ),
    'text/x-script.scheme' => array(
        'extension' => '.scm',
        'label' => 'Scheme File',
    ),
    'text/x-scheme' => array(
        'extension' => '.scm',
        'label' => 'Scheme File',
    ),
    'text/scriptlet' => array(
        'extension' => '.sct',
        'label' => 'Windows Script Component',
    ),
    'application/spss' => array(
        'extension' => '.sd2',
        'label' => 'SPSS File',
    ),
    'application/x-sea' => array(
        'extension' => '.sea',
        'label' => 'Self-extracting Archive',
    ),
    'application/x-sh' => array(
        'extension' => '.sh',
        'label' => 'Shell Script File',
    ),
    'application/x-shellscript' => array(
        'extension' => '.sh',
        'label' => 'Shell Script File',
    ),
    'application/x-shar' => array(
        'extension' => '.shar',
        'label' => 'UNIX Shar Archive File',
    ),
    'text/html' => array(
        'extension' => '.shtml',
        'label' => 'HTML File',
    ),
    'application/xhtml+xml' => array(
        'extension' => '.shtml',
        'label' => 'HTML File',
    ),
    'application/stuffit' => array(
        'extension' => '.sit',
        'label' => 'Stuffit Archive',
    ),
    'application/x-sit' => array(
        'extension' => '.sit',
        'label' => 'Stuffit Archive',
    ),
    'application/x-stuffit' => array(
        'extension' => '.sit',
        'label' => 'Stuffit Archive',
    ),
    'application/smil' => array(
        'extension' => '.smil',
        'label' => 'SMIL File',
    ),
    'application/smil+xml' => array(
        'extension' => '.smil',
        'label' => 'SMIL File',
    ),
    'audio/basic' => array(
        'extension' => '.snd',
        'label' => 'Audio File',
    ),
    'audio/au' => array(
        'extension' => '.snd',
        'label' => 'Audio File',
    ),
    'audio/x-au' => array(
        'extension' => '.snd',
        'label' => 'Audio File',
    ),
    'audio/x-basic' => array(
        'extension' => '.snd',
        'label' => 'Audio File',
    ),
    'application/x-shockwave-flash' => array(
        'extension' => '.spl',
        'label' => 'Adobe Flash File',
    ),
    'application/futuresplash' => array(
        'extension' => '.spl',
        'label' => 'Adobe Flash File',
    ),
    'application/spss' => array(
        'extension' => '.spo',
        'label' => 'SPSS File',
    ),
    'text/x-sql' => array(
        'extension' => '.sql',
        'label' => 'SQL File',
    ),
    'text/sql' => array(
        'extension' => '.sql',
        'label' => 'SQL File',
    ),
    'application/x-wais-source' => array(
        'extension' => '.src',
        'label' => 'WAIS Source File',
    ),
    'application/vnd.ms-pkicertstore' => array(
        'extension' => '.sst',
        'label' => 'Certificate Store Crypto Shell Extension',
    ),
    'application/vnd.ms-pkistl' => array(
        'extension' => '.stl',
        'label' => 'Certificate Trust List',
    ),
    'text/html' => array(
        'extension' => '.stm',
        'label' => 'SHTML File',
    ),
    'application/x-director' => array(
        'extension' => '.swa',
        'label' => 'Adobe Director File',
    ),
    'application/x-shockwave-flash' => array(
        'extension' => '.swf',
        'label' => 'Adobe Flash File',
    ),
    'application/futuresplash' => array(
        'extension' => '.swf',
        'label' => 'Adobe Flash File',
    ),
    'application/vnd.sun.xml.writer' => array(
        'extension' => '.sxw',
        'label' => 'OpenOffice.org Document',
    ),
    'application/x-troff' => array(
        'extension' => '.t',
        'label' => 'Troff File',
    ),
    'application/tar' => array(
        'extension' => '.tar',
        'label' => 'Compressed Tar File',
    ),
    'application/x-gtar' => array(
        'extension' => '.tar',
        'label' => 'Compressed Tar File',
    ),
    'application/x-tar' => array(
        'extension' => '.tar',
        'label' => 'Compressed Tar File',
    ),
    'application/x-tcl' => array(
        'extension' => '.tcl',
        'label' => 'TCL Source File',
    ),
    'text/x-script.tcl' => array(
        'extension' => '.tcl',
        'label' => 'TCL Source File',
    ),
    'text/x-tcl' => array(
        'extension' => '.tcl',
        'label' => 'TCL Source File',
    ),
    'application/x-tex' => array(
        'extension' => '.tex',
        'label' => 'TeX File',
    ),
    'text/x-tex' => array(
        'extension' => '.tex',
        'label' => 'TeX File',
    ),
    'image/x-targa' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'application/tga' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'application/x-targa' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'application/x-tga' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'image/targa' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'image/tga' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'image/x-tga' => array(
        'extension' => '.tga',
        'label' => 'Truevision Targa Image',
    ),
    'application/gzip' => array(
        'extension' => '.tgz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/gzip-compressed' => array(
        'extension' => '.tgz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/gzipped' => array(
        'extension' => '.tgz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/x-gunzip' => array(
        'extension' => '.tgz',
        'label' => 'Compressed Gzip Archive',
    ),
    'application/x-gzip' => array(
        'extension' => '.tgz',
        'label' => 'Compressed Gzip Archive',
    ),
    'image/tiff' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'application/tif' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'application/tiff' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'application/x-tif' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'application/x-tiff' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'image/tif' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'image/x-tif' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'image/x-tiff' => array(
        'extension' => '.tif',
        'label' => 'Tagged Image File',
    ),
    'image/tiff' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'application/tif' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'application/tiff' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'application/x-tif' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'application/x-tiff' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'image/tif' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'image/x-tif' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'image/x-tiff' => array(
        'extension' => '.tiff',
        'label' => 'Tagged Image File',
    ),
    'application/ms-tnef' => array(
        'extension' => '.tnef',
        'label' => 'Microsoft Exchange TNEF File',
    ),
    'application/x-troff' => array(
        'extension' => '.tr',
        'label' => 'Troff File',
    ),
    'application/x-msterminal' => array(
        'extension' => '.trm',
        'label' => 'Terminal Settings',
    ),
    'text/tsv' => array(
        'extension' => '.tsv',
        'label' => 'Tab-delimited File',
    ),
    'text/tab-separated-values' => array(
        'extension' => '.tsv',
        'label' => 'Tab-delimited File',
    ),
    'text/x-tab-separated-values' => array(
        'extension' => '.tsv',
        'label' => 'Tab-delimited File',
    ),
    'application/twb' => array(
        'extension' => '.twb',
        'label' => 'Tableau Workbook File',
    ),
    'application/twbx' => array(
        'extension' => '.twb',
        'label' => 'Tableau Workbook File',
    ),
    'application/x-twb' => array(
        'extension' => '.twb',
        'label' => 'Tableau Workbook File',
    ),
    'application/twb' => array(
        'extension' => '.twbx',
        'label' => 'Tableau Workbook File',
    ),
    'application/twbx' => array(
        'extension' => '.twbx',
        'label' => 'Tableau Workbook File',
    ),
    'application/x-twb' => array(
        'extension' => '.twbx',
        'label' => 'Tableau Workbook File',
    ),
    'text/plain' => array(
        'extension' => '.txt',
        'label' => 'Plain Text File',
    ),
    'text/iuls' => array(
        'extension' => '.uls',
        'label' => 'Internet Location Service',
    ),
    'application/x-ustar' => array(
        'extension' => '.ustar',
        'label' => 'POSIX Tar Compressed Archive',
    ),
    'text/x-vcard' => array(
        'extension' => '.vcf',
        'label' => 'VCard File',
    ),
    'x-world/x-vrml' => array(
        'extension' => '.vrml',
        'label' => 'Virtual Reality Modeling Language File',
    ),
    'application/vnd.visio' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'application/visio' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'application/visio.drawing' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'application/vsd' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'application/x-visio' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'application/x-vsd' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'image/x-vsd' => array(
        'extension' => '.vsd',
        'label' => 'Microsoft Visio File',
    ),
    'application/x-director' => array(
        'extension' => '.w3d',
        'label' => 'Adobe Director File',
    ),
    'application/x-webarchive' => array(
        'extension' => '.war',
        'label' => 'KDE Web Archive',
    ),
    'audio/wav' => array(
        'extension' => '.wav',
        'label' => 'Waveform Audio File',
    ),
    'audio/s-wav' => array(
        'extension' => '.wav',
        'label' => 'Waveform Audio File',
    ),
    'audio/wave' => array(
        'extension' => '.wav',
        'label' => 'Waveform Audio File',
    ),
    'audio/x-wav' => array(
        'extension' => '.wav',
        'label' => 'Waveform Audio File',
    ),
    'application/vnd.ms-works' => array(
        'extension' => '.wcm',
        'label' => 'Works File Transmission',
    ),
    'application/vnd.ms-works' => array(
        'extension' => '.wdb',
        'label' => 'Microsoft Works File',
    ),
    'application/x-msworks-wp' => array(
        'extension' => '.wdb',
        'label' => 'Microsoft Works File',
    ),
    'application/vnd.ms-works' => array(
        'extension' => '.wks',
        'label' => 'Microsoft Works File',
    ),
    'application/x-msworks-wp' => array(
        'extension' => '.wks',
        'label' => 'Microsoft Works File',
    ),
    'audio/x-ms-wma' => array(
        'extension' => '.wma',
        'label' => 'Windows Media File',
    ),
    'image/x-wmf' => array(
        'extension' => '.wmf',
        'label' => 'Windows Media File',
    ),
    'application/wmf' => array(
        'extension' => '.wmf',
        'label' => 'Windows Media File',
    ),
    'application/x-msmetafile' => array(
        'extension' => '.wmf',
        'label' => 'Windows Media File',
    ),
    'application/x-wmf' => array(
        'extension' => '.wmf',
        'label' => 'Windows Media File',
    ),
    'image/wmf' => array(
        'extension' => '.wmf',
        'label' => 'Windows Media File',
    ),
    'image/x-win-metafile' => array(
        'extension' => '.wmf',
        'label' => 'Windows Media File',
    ),
    'video/x-ms-wmv' => array(
        'extension' => '.wmv',
        'label' => 'Windows Media File',
    ),
    'application/x-ms-wmz' => array(
        'extension' => '.wmz',
        'label' => 'Windows Media Compressed File',
    ),
    'application/wordperfect' => array(
        'extension' => '.wpd',
        'label' => 'WordPerfect Document',
    ),
    'application/wordperf' => array(
        'extension' => '.wpd',
        'label' => 'WordPerfect Document',
    ),
    'application/wpd' => array(
        'extension' => '.wpd',
        'label' => 'WordPerfect Document',
    ),
    'application/vnd.ms-works' => array(
        'extension' => '.wps',
        'label' => 'Microsoft Works File',
    ),
    'application/x-msworks-wp' => array(
        'extension' => '.wps',
        'label' => 'Microsoft Works File',
    ),
    'application/x-mswrite' => array(
        'extension' => '.wri',
        'label' => 'Write Document',
    ),
    'x-world/x-vrml' => array(
        'extension' => '.wrl',
        'label' => 'VRML 3D File',
    ),
    'x-world/x-vrml' => array(
        'extension' => '.wrz',
        'label' => 'VRML 3D File',
    ),
    'image/x-xbitmap' => array(
        'extension' => '.xbm',
        'label' => 'Bitmap Image',
    ),
    'text/html' => array(
        'extension' => '.xhtml',
        'label' => 'HTML File',
    ),
    'application/xhtml+xml' => array(
        'extension' => '.xhtml',
        'label' => 'HTML File',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xla',
        'label' => 'Excel Add-in',
    ),
    'application/vnd.ms-excel.addin.macroEnabled.12' => array(
        'extension' => '.xlam',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xlc',
        'label' => 'Excel Chart',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/excel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/msexcel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/msexcell' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-dos_ms_excel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-excel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-ms-excel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-msexcel' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-xls' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/xls' => array(
        'extension' => '.xll',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xlm',
        'label' => 'Excel Macro File',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/excel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/msexcel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/msexcell' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-dos_ms_excel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-excel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-ms-excel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-msexcel' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/x-xls' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/xls' => array(
        'extension' => '.xls',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.ms-excel.sheet.binary.macroEnabled.12' => array(
        'extension' => '.xlsb',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.ms-excel.sheet.macroEnabled.12' => array(
        'extension' => '.xlsm',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => array(
        'extension' => '.xlsx',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xlt',
        'label' => 'Excel Template File',
    ),
    'application/vnd.ms-excel.template.macroEnabled.12' => array(
        'extension' => '.xltm',
        'label' => 'Microsoft Excel File',
    ),
    'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => array(
        'extension' => '.xltx',
        'label' => 'Microsoft Excel Template File',
    ),
    'application/vnd.ms-excel' => array(
        'extension' => '.xlw',
        'label' => 'Excel Workspace File',
    ),
    'text/xml' => array(
        'extension' => '.xml',
        'label' => 'XML File',
    ),
    'application/x-xml' => array(
        'extension' => '.xml',
        'label' => 'XML File',
    ),
    'application/xml' => array(
        'extension' => '.xml',
        'label' => 'XML File',
    ),
    'image/x-xpixmap' => array(
        'extension' => '.xpm',
        'label' => 'Pixmap Image',
    ),
    'image/x-xpm' => array(
        'extension' => '.xpm',
        'label' => 'Pixmap Image',
    ),
    'image/xpm' => array(
        'extension' => '.xpm',
        'label' => 'Pixmap Image',
    ),
    'application/vnd.ms-xpsdocument' => array(
        'extension' => '.xps',
        'label' => 'Microsoft XPS File',
    ),
    'text/xsl' => array(
        'extension' => '.xsl',
        'label' => 'XSLT Stylesheet File',
    ),
    'image/x-xwindowdump' => array(
        'extension' => '.xwd',
        'label' => 'X Windows Dump',
    ),
    'image/xwd' => array(
        'extension' => '.xwd',
        'label' => 'X Windows Dump',
    ),
    'image/x-xwd' => array(
        'extension' => '.xwd',
        'label' => 'X Windows Dump',
    ),
    'application/xwd' => array(
        'extension' => '.xwd',
        'label' => 'X Windows Dump',
    ),
    'application/x-xwd' => array(
        'extension' => '.xwd',
        'label' => 'X Windows Dump',
    ),
    'application/x-compress' => array(
        'extension' => '.z',
        'label' => 'UNIX Compressed Archive File',
    ),
    'application/z' => array(
        'extension' => '.z',
        'label' => 'UNIX Compressed Archive File',
    ),
    'application/x-z' => array(
        'extension' => '.z',
        'label' => 'UNIX Compressed Archive File',
    ),
    'application/zip' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
    'application/x-compress' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
    'application/x-compressed' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
    'application/x-zip' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
    'application/x-zip-compressed' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
    'application/zip-compressed' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
    'application/x-7zip-compressed' => array(
        'extension' => '.zip',
        'label' => 'Compressed Zip Archive',
    ),
);