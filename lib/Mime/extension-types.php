<?php
return array(
    '.323' => array(
        'mimeTypes' => array(
            'text/h323'
        ),
        'label' => 'Internet Telephony'
    ),
    '.aac' => array(
        'mimeTypes' => array(
            'audio/aac'
        ),
        'label' => 'AAC Audio File'
    ),
    '.abw' => array(
        'mimeTypes' => array(
            'application/abiword'
        ),
        'label' => 'AbiWord Document'
    ),
    '.acx' => array(
        'mimeTypes' => array(
            'application/internet-property-stream'
        ),
        'label' => 'Atari ST Executable'
    ),
    '.ai' => array(
        'mimeTypes' => array(
            'application/illustrator'
        ),
        'label' => 'Adobe Illustrator File'
    ),
    '.aif' => array(
        'mimeTypes' => array(
            'audio/aiff',
            'audio/aifc',
            'audio/x-aiff'
        ),
        'label' => 'AIFF Audio File'
    ),
    '.aifc' => array(
        'mimeTypes' => array(
            'audio/aiff',
            'audio/aifc',
            'audio/x-aiff'
        ),
        'label' => 'AIFF Audio File'
    ),
    '.aiff' => array(
        'mimeTypes' => array(
            'audio/aiff',
            'audio/aifc',
            'audio/x-aiff'
        ),
        'label' => 'AIFF Audio File'
    ),
    '.asf' => array(
        'mimeTypes' => array(
            'video/x-ms-asf'
        ),
        'label' => 'Windows Media File'
    ),
    '.asp' => array(
        'mimeTypes' => array(
            'application/x-asp',
            'text/asp'
        ),
        'label' => 'ASP Source File'
    ),
    '.asr' => array(
        'mimeTypes' => array(
            'video/x-ms-asf'
        ),
        'label' => 'Windows Media File'
    ),
    '.asx' => array(
        'mimeTypes' => array(
            'video/x-ms-asf'
        ),
        'label' => 'Windows Media File'
    ),
    '.au' => array(
        'mimeTypes' => array(
            'audio/basic',
            'audio/au',
            'audio/x-au',
            'audio/x-basic'
        ),
        'label' => 'Audio File'
    ),
    '.avi' => array(
        'mimeTypes' => array(
            'video/avi',
            'application/x-troff-msvideo',
            'image/avi',
            'video/msvideo',
            'video/x-msvideo',
            'video/xmpg2'
        ),
        'label' => 'AVI Video File'
    ),
    '.axs' => array(
        'mimeTypes' => array(
            'application/olescript'
        ),
        'label' => 'ActiveX Script'
    ),
    '.bas' => array(
        'mimeTypes' => array(
            'text/plain'
        ),
        'label' => 'BASIC Source Code'
    ),
    '.bin' => array(
        'mimeTypes' => array(
            'application/octet-stream',
            'application/bin',
            'application/binary',
            'application/x-msdownload'
        ),
        'label' => 'Binary File'
    ),
    '.bmp' => array(
        'mimeTypes' => array(
            'image/bmp',
            'application/bmp',
            'application/x-bmp',
            'image/ms-bmp',
            'image/x-bitmap',
            'image/x-bmp',
            'image/x-ms-bmp',
            'image/x-win-bitmap',
            'image/x-windows-bmp',
            'image/x-xbitmap'
        ),
        'label' => 'Bitmap Image'
    ),
    '.bz2' => array(
        'mimeTypes' => array(
            'application/x-bzip2',
            'application/bzip2',
            'application/x-bz2',
            'application/x-bzip'
        ),
        'label' => 'Compressed Bzip2 File'
    ),
    '.c' => array(
        'mimeTypes' => array(
            'text/x-csrc'
        ),
        'label' => 'C Source File'
    ),
    '.c++' => array(
        'mimeTypes' => array(
            'text/x-c++src'
        ),
        'label' => 'C++ Source File'
    ),
    '.cab' => array(
        'mimeTypes' => array(
            'application/vnd.ms-cab-compressed',
            'application/cab',
            'application/x-cabinet'
        ),
        'label' => 'Microsoft Cabinet Archive'
    ),
    '.cat' => array(
        'mimeTypes' => array(
            'application/vnd.ms-pkiseccat'
        ),
        'label' => 'Security Catalog'
    ),
    '.cct' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.cdf' => array(
        'mimeTypes' => array(
            'application/cdf',
            'application/x-cdf',
            'application/netcdf',
            'application/x-netcdf',
            'text/cdf',
            'text/x-cdf'
        ),
        'label' => 'Channel Definition Format'
    ),
    '.cer' => array(
        'mimeTypes' => array(
            'application/x-x509-ca-cert',
            'application/pkix-cert',
            'application/x-pkcs12',
            'application/keychain_access'
        ),
        'label' => 'Internet Security Certificate File'
    ),
    '.cfc' => array(
        'mimeTypes' => array(
            'application/x-cfm'
        ),
        'label' => 'ColdFusion Source File'
    ),
    '.cfm' => array(
        'mimeTypes' => array(
            'application/x-cfm'
        ),
        'label' => 'ColdFusion Source File'
    ),
    '.class' => array(
        'mimeTypes' => array(
            'application/x-java',
            'application/java',
            'application/java-byte-code',
            'application/java-vm',
            'application/x-java-applet',
            'application/x-java-bean',
            'application/x-java-class',
            'application/x-java-vm',
            'application/x-jinit-bean',
            'application/x-jinit-applet'
        ),
        'label' => 'Java Bytecode File'
    ),
    '.clp' => array(
        'mimeTypes' => array(
            'application/x-msclip'
        ),
        'label' => 'Windows Clipboard/Picture'
    ),
    '.cmx' => array(
        'mimeTypes' => array(
            'image/x-cmx',
            'application/cmx',
            'application/x-cmx',
            'drawing/cmx',
            'image/x-cmx'
        ),
        'label' => 'Presentation Exchange Image'
    ),
    '.cod' => array(
        'mimeTypes' => array(
            'image/cis-cod'
        ),
        'label' => 'CIS-COD File'
    ),
    '.cp' => array(
        'mimeTypes' => array(
            'text/x-c++src'
        ),
        'label' => 'C++ Source File'
    ),
    '.cpio' => array(
        'mimeTypes' => array(
            'application/x-cpio'
        ),
        'label' => 'UNIX CPIO Archive'
    ),
    '.cpp' => array(
        'mimeTypes' => array(
            'text/x-c++src'
        ),
        'label' => 'C++ Source File'
    ),
    '.crd' => array(
        'mimeTypes' => array(
            'application/x-mscardfile'
        ),
        'label' => 'Windows Cardfile'
    ),
    '.crt' => array(
        'mimeTypes' => array(
            'application/x-x509-ca-cert',
            'application/pkix-cert',
            'application/x-pkcs12',
            'application/keychain_access'
        ),
        'label' => 'Internet Security Certificate File'
    ),
    '.crl' => array(
        'mimeTypes' => array(
            'application/pkix-crl'
        ),
        'label' => 'Certificate Revocation List'
    ),
    '.csh' => array(
        'mimeTypes' => array(
            'application/x-csh'
        ),
        'label' => 'C Shell File'
    ),
    '.css' => array(
        'mimeTypes' => array(
            'text/css',
            'application/css-stylesheet'
        ),
        'label' => 'Cascading Stylesheet File'
    ),
    '.cst' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.csv' => array(
        'mimeTypes' => array(
            'text/csv',
            'application/csv',
            'text/comma-separated-values',
            'text/x-comma-separated-values'
        ),
        'label' => 'Comma-delimited File'
    ),
    '.cxt' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.dcr' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.der' => array(
        'mimeTypes' => array(
            'application/x-x509-ca-cert',
            'application/pkix-cert',
            'application/x-pkcs12',
            'application/keychain_access'
        ),
        'label' => 'Internet Security Certificate File'
    ),
    '.dib' => array(
        'mimeTypes' => array(
            'image/bmp',
            'application/bmp',
            'application/x-bmp',
            'image/ms-bmp',
            'image/x-bitmap',
            'image/x-bmp',
            'image/x-ms-bmp',
            'image/x-win-bitmap',
            'image/x-windows-bmp',
            'image/x-xbitmap'
        ),
        'label' => 'Bitmap Image'
    ),
    '.diff' => array(
        'mimeTypes' => array(
            'text/x-patch'
        ),
        'label' => 'Patch Source File'
    ),
    '.dir' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.dll' => array(
        'mimeTypes' => array(
            'application/x-msdownload',
            'application/octet-stream',
            'application/x-msdos-program'
        ),
        'label' => 'Dynamic Link Library'
    ),
    '.dms' => array(
        'mimeTypes' => array(
            'application/octet-stream'
        ),
        'label' => 'DISKMASHER Compressed Archive'
    ),
    '.doc' => array(
        'mimeTypes' => array(
            'application/vnd.ms-word',
            'application/doc',
            'application/msword',
            'application/msword-doc',
            'application/vnd.msword',
            'application/winword',
            'application/word',
            'application/x-msw6',
            'application/x-msword',
            'application/x-msword-doc'
        ),
        'label' => 'Microsoft Word Document'
    ),
    '.docm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-word.document.macroEnabled.12'
        ),
        'label' => 'Microsoft Word Document'
    ),
    '.docx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.ms-word.document.12',
            'application/vnd.openxmlformats-officedocument.word'
        ),
        'label' => 'Microsoft Word File'
    ),
    '.dot' => array(
        'mimeTypes' => array(
            'application/msword'
        ),
        'label' => 'Word Document Template'
    ),
    '.dotm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-word.template.macroEnabled.12'
        ),
        'label' => 'Microsoft Word Template File'
    ),
    '.dotx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
        ),
        'label' => 'Microsoft Word Template File'
    ),
    '.dta' => array(
        'mimeTypes' => array(
            'application/x-stata'
        ),
        'label' => 'Stata Data File'
    ),
    '.dv' => array(
        'mimeTypes' => array(
            'video/x-dv'
        ),
        'label' => 'Digital Video File'
    ),
    '.dvi' => array(
        'mimeTypes' => array(
            'application/x-dvi'
        ),
        'label' => 'DVI File'
    ),
    '.dwg' => array(
        'mimeTypes' => array(
            'image/x-dwg',
            'application/acad',
            'application/autocad_dwg',
            'application/dwg',
            'application/x-acad',
            'application/x-autocad',
            'application/x-dwg',
            'image/vnd.dwg'
        ),
        'label' => 'AutoCAD Drawing'
    ),
    '.dxf' => array(
        'mimeTypes' => array(
            'application/x-autocad',
            'application/dxf',
            'application/x-dxf',
            'drawing/x-dxf',
            'image/vnd.dxf',
            'image/x-autocad',
            'image/x-dxf'
        ),
        'label' => 'AutoCAD Drawing'
    ),
    '.dxr' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.elc' => array(
        'mimeTypes' => array(
            'application/x-elc'
        ),
        'label' => 'Emacs Source File'
    ),
    '.eml' => array(
        'mimeTypes' => array(
            'message/rfc822'
        ),
        'label' => 'Web Archive File'
    ),
    '.enl' => array(
        'mimeTypes' => array(
            'application/x-endnote-library',
            'application/x-endnote-refer'
        ),
        'label' => 'EndNote Library File'
    ),
    '.enz' => array(
        'mimeTypes' => array(
            'application/x-endnote-library',
            'application/x-endnote-refer'
        ),
        'label' => 'EndNote Library File'
    ),
    '.eps' => array(
        'mimeTypes' => array(
            'application/postscript',
            'application/eps',
            'application/x-eps',
            'image/eps',
            'image/x-eps'
        ),
        'label' => 'PostScript File'
    ),
    '.etx' => array(
        'mimeTypes' => array(
            'text/x-setext',
            'text/anytext'
        ),
        'label' => 'Setext (Structure Enhanced Text)'
    ),
    '.evy' => array(
        'mimeTypes' => array(
            'application/envoy',
            'application/x-envoy'
        ),
        'label' => 'Envoy Document'
    ),
    '.exe' => array(
        'mimeTypes' => array(
            'application/x-msdos-program',
            'application/dos-exe',
            'application/exe',
            'application/msdos-windows',
            'application/x-sdlc',
            'application/x-exe',
            'application/x-winexe'
        ),
        'label' => 'Windows Executable File'
    ),
    '.fif' => array(
        'mimeTypes' => array(
            'application/fractals',
            'image/fif'
        ),
        'label' => 'Fractal Image Format'
    ),
    '.flr' => array(
        'mimeTypes' => array(
            'x-world/x-vrml'
        ),
        'label' => 'Virtual Reality Modeling Language File'
    ),
    '.fm' => array(
        'mimeTypes' => array(
            'application/vnd.framemaker',
            'application/framemaker',
            'application/maker',
            'application/vnd.mif',
            'application/x-framemaker',
            'application/x-maker',
            'application/x-mif'
        ),
        'label' => 'FrameMaker File'
    ),
    '.fqd' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.gif' => array(
        'mimeTypes' => array(
            'image/gif'
        ),
        'label' => 'GIF Image'
    ),
    '.gtar' => array(
        'mimeTypes' => array(
            'application/tar',
            'application/x-gtar',
            'application/x-tar'
        ),
        'label' => 'Compressed Tar File'
    ),
    '.gz' => array(
        'mimeTypes' => array(
            'application/gzip',
            'application/gzip-compressed',
            'application/gzipped',
            'application/x-gunzip',
            'application/x-gzip'
        ),
        'label' => 'Compressed Gzip Archive'
    ),
    '.h' => array(
        'mimeTypes' => array(
            'text/x-chdr'
        ),
        'label' => 'C Header File'
    ),
    '.hdf' => array(
        'mimeTypes' => array(
            'application/x-hdf'
        ),
        'label' => 'Hierarchical Data Format File'
    ),
    '.hlp' => array(
        'mimeTypes' => array(
            'application/winhlp',
            'application/x-helpfile',
            'application/x-winhelp'
        ),
        'label' => 'Windows Help File'
    ),
    '.hqx' => array(
        'mimeTypes' => array(
            'application/binhex',
            'application/mac-binhex',
            'application/mac-binhex40'
        ),
        'label' => 'BinHex Archive'
    ),
    '.hta' => array(
        'mimeTypes' => array(
            'application/hta'
        ),
        'label' => 'HTML Application File'
    ),
    '.htc' => array(
        'mimeTypes' => array(
            'text/x-component'
        ),
        'label' => 'HTML Component File'
    ),
    '.htm' => array(
        'mimeTypes' => array(
            'text/html',
            'application/xhtml+xml'
        ),
        'label' => 'HTML File'
    ),
    '.html' => array(
        'mimeTypes' => array(
            'text/html',
            'application/xhtml+xml'
        ),
        'label' => 'HTML File'
    ),
    '.htt' => array(
        'mimeTypes' => array(
            'text/webviewhtml'
        ),
        'label' => 'Hypertext Template File'
    ),
    '.ico' => array(
        'mimeTypes' => array(
            'image/x-ico'
        ),
        'label' => 'Favicon, Icon File'
    ),
    '.ics' => array(
        'mimeTypes' => array(
            'text/calendar'
        ),
        'label' => 'Calendar File'
    ),
    '.ief' => array(
        'mimeTypes' => array(
            'image/ief'
        ),
        'label' => 'IEF Image File'
    ),
    '.iii' => array(
        'mimeTypes' => array(
            'application/x-iphone'
        ),
        'label' => 'Intel IPhone Compatible File'
    ),
    '.indd' => array(
        'mimeTypes' => array(
            'application/x-indesign'
        ),
        'label' => 'Adobe InDesign File'
    ),
    '.ins' => array(
        'mimeTypes' => array(
            'application/x-internet-signup'
        ),
        'label' => 'IIS Internet Communications Settings File'
    ),
    '.isp' => array(
        'mimeTypes' => array(
            'application/x-internet-signup'
        ),
        'label' => 'IIS Internet Service Provider Settings File'
    ),
    '.jad' => array(
        'mimeTypes' => array(
            'text/vnd.sun.j2me.app-descriptor'
        ),
        'label' => 'Java Application Descriptor'
    ),
    '.jar' => array(
        'mimeTypes' => array(
            'application/java-archive'
        ),
        'label' => 'Java Archive File'
    ),
    '.java' => array(
        'mimeTypes' => array(
            'text/x-java',
            'java/*',
            'text/java',
            'text/x-java-source'
        ),
        'label' => 'Java Source File'
    ),
    '.jfif' => array(
        'mimeTypes' => array(
            'image/jpeg',
            'image/pjpeg'
        ),
        'label' => 'JPEG Image'
    ),
    '.jpe' => array(
        'mimeTypes' => array(
            'image/jpeg',
            'image/pjpeg'
        ),
        'label' => 'JPEG Image'
    ),
    '.jpeg' => array(
        'mimeTypes' => array(
            'image/jpeg',
            'image/pjpeg'
        ),
        'label' => 'JPEG Image'
    ),
    '.jpg' => array(
        'mimeTypes' => array(
            'image/jpeg',
            'image/pjpeg'
        ),
        'label' => 'JPEG Image'
    ),
    '.js' => array(
        'mimeTypes' => array(
            'text/javascript',
            'application/javascript',
            'application/x-javascript',
            'application/x-js'
        ),
        'label' => 'JavaScript Source File'
    ),
    '.kml' => array(
        'mimeTypes' => array(
            'application/vnd.google-earth.kml+xml'
        ),
        'label' => 'KML File'
    ),
    '.kmz' => array(
        'mimeTypes' => array(
            'application/vnd.google-earth.kmz'
        ),
        'label' => 'Compressed KML File'
    ),
    '.latex' => array(
        'mimeTypes' => array(
            'application/x-latex',
            'text/x-latex'
        ),
        'label' => 'LATEX File'
    ),
    '.lha' => array(
        'mimeTypes' => array(
            'application/x-lha',
            'application/lha',
            'application/lzh',
            'application/x-lzh',
            'application/x-lzh-archive'
        ),
        'label' => 'Compressed Archive File'
    ),
    '.lib' => array(
        'mimeTypes' => array(
            'application/x-endnote-library',
            'application/x-endnote-refer'
        ),
        'label' => 'EndNote Library File'
    ),
    '.llb' => array(
        'mimeTypes' => array(
            'application/x-labview',
            'application/x-labview-vi'
        ),
        'label' => 'LabVIEW Application File'
    ),
    '.log' => array(
        'mimeTypes' => array(
            'text/x-log'
        ),
        'label' => 'Log Text File'
    ),
    '.lsf' => array(
        'mimeTypes' => array(
            'video/x-la-asf'
        ),
        'label' => 'Streaming Audio/Video File'
    ),
    '.lsx' => array(
        'mimeTypes' => array(
            'video/x-la-asf'
        ),
        'label' => 'Streaming Audio/Video File'
    ),
    '.lvx' => array(
        'mimeTypes' => array(
            'application/x-labview-exec'
        ),
        'label' => 'LabVIEW CAL Simulation File'
    ),
    '.lzh' => array(
        'mimeTypes' => array(
            'application/x-lha',
            'application/lha',
            'application/lzh',
            'application/x-lzh',
            'application/x-lzh-archive'
        ),
        'label' => 'Compressed Archive File'
    ),
    '.m' => array(
        'mimeTypes' => array(
            'text/x-objcsrc'
        ),
        'label' => 'Objective C Source File'
    ),
    '.m1v' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Video File'
    ),
    '.m2v' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Video File'
    ),
    '.m3u' => array(
        'mimeTypes' => array(
            'audio/x-mpegurl',
            'application/x-winamp-playlist',
            'audio/mpegurl',
            'audio/mpeg-url',
            'audio/playlist',
            'audio/scpls',
            'audio/x-scpls'
        ),
        'label' => 'MP3 Playlist File'
    ),
    '.m4a' => array(
        'mimeTypes' => array(
            'audio/m4a',
            'audio/x-m4a'
        ),
        'label' => 'MPEG-4 Audio File'
    ),
    '.m4v' => array(
        'mimeTypes' => array(
            'video/mp4',
            'video/mpeg4',
            'video/x-m4v'
        ),
        'label' => 'MPEG-4 Video File'
    ),
    '.ma' => array(
        'mimeTypes' => array(
            'application/mathematica'
        ),
        'label' => 'Mathematica File'
    ),
    '.mail' => array(
        'mimeTypes' => array(
            'message/rfc822'
        ),
        'label' => 'Web Archive File'
    ),
    '.man' => array(
        'mimeTypes' => array(
            'application/x-troff-man'
        ),
        'label' => 'Troff With MAN Macros File'
    ),
    '.mcd' => array(
        'mimeTypes' => array(
            'application/x-mathcad',
            'application/mcad'
        ),
        'label' => 'MathCaD File'
    ),
    '.mdb' => array(
        'mimeTypes' => array(
            'application/vnd.ms-access',
            'application/mdb',
            'application/msaccess',
            'application/vnd.msaccess',
            'application/x-mdb',
            'application/x-msaccess'
        ),
        'label' => 'Microsoft Access File'
    ),
    '.me' => array(
        'mimeTypes' => array(
            'application/x-troff-me'
        ),
        'label' => 'Troff With ME Macros File'
    ),
    '.mfp' => array(
        'mimeTypes' => array(
            'application/x-shockwave-flash',
            'application/futuresplash'
        ),
        'label' => 'Adobe Flash File'
    ),
    '.mht' => array(
        'mimeTypes' => array(
            'message/rfc822'
        ),
        'label' => 'Web Archive File'
    ),
    '.mhtml' => array(
        'mimeTypes' => array(
            'message/rfc822'
        ),
        'label' => 'Web Archive File'
    ),
    '.mid' => array(
        'mimeTypes' => array(
            'audio/x-midi',
            'application/x-midi',
            'audio/mid',
            'audio/midi',
            'audio/soundtrack'
        ),
        'label' => 'MIDI Audio File'
    ),
    '.midi' => array(
        'mimeTypes' => array(
            'audio/x-midi',
            'application/x-midi',
            'audio/mid',
            'audio/midi',
            'audio/soundtrack'
        ),
        'label' => 'MIDI Audio File'
    ),
    '.mif' => array(
        'mimeTypes' => array(
            'application/vnd.framemaker',
            'application/framemaker',
            'application/maker',
            'application/vnd.mif',
            'application/x-framemaker',
            'application/x-maker',
            'application/x-mif'
        ),
        'label' => 'FrameMaker File'
    ),
    '.mny' => array(
        'mimeTypes' => array(
            'application/x-msmoney'
        ),
        'label' => 'Money Data File'
    ),
    '.mov' => array(
        'mimeTypes' => array(
            'video/quicktime'
        ),
        'label' => 'Quicktime Video File'
    ),
    '.mp2' => array(
        'mimeTypes' => array(
            'video/mpeg',
            'audio/mpeg',
            'audio/x-mpeg',
            'audio/x-mpeg-2',
            'video/x-mpeg',
            'video/x-mpeq2a'
        ),
        'label' => 'MPEG Layer 2 Audio File'
    ),
    '.mp3' => array(
        'mimeTypes' => array(
            'audio/mpeg',
            'audio/mp3',
            'audio/mpeg3',
            'audio/mpg',
            'audio/x-mp3',
            'audio/x-mpeg',
            'audio/x-mpeg3',
            'audio/x-mpg'
        ),
        'label' => 'MP3 Audio File'
    ),
    '.mpa' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Audio Stream'
    ),
    '.mpe' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Video File'
    ),
    '.mpeg' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Video File'
    ),
    '.mpg' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Video File'
    ),
    '.mpp' => array(
        'mimeTypes' => array(
            'application/vnd.ms-project',
            'application/mpp',
            'application/msproj',
            'application/msproject',
            'application/x-dos_ms_project',
            'application/x-ms-project',
            'application/x-msproject'
        ),
        'label' => 'Microsoft Project File'
    ),
    '.mpv2' => array(
        'mimeTypes' => array(
            'video/mpeg'
        ),
        'label' => 'MPEG Audio Stream'
    ),
    '.mqv' => array(
        'mimeTypes' => array(
            'video/quicktime'
        ),
        'label' => 'Quicktime Video File'
    ),
    '.ms' => array(
        'mimeTypes' => array(
            'application/x-troff-ms'
        ),
        'label' => 'Troff With MS Macros File'
    ),
    '.mvb' => array(
        'mimeTypes' => array(
            'application/x-msmediaview'
        ),
        'label' => 'Multimedia Viewer'
    ),
    '.mws' => array(
        'mimeTypes' => array(
            'application/x-maple',
            'application/maple-v-r4'
        ),
        'label' => 'Maple Worksheet File'
    ),
    '.nb' => array(
        'mimeTypes' => array(
            'application/mathematica'
        ),
        'label' => 'Mathematica File'
    ),
    '.nws' => array(
        'mimeTypes' => array(
            'message/rfc822'
        ),
        'label' => 'Web Archive File'
    ),
    '.oda' => array(
        'mimeTypes' => array(
            'application/oda'
        ),
        'label' => 'ODA Document'
    ),
    '.odf' => array(
        'mimeTypes' => array(
            'application/vnd.oasis.opendocument.formula'
        ),
        'label' => 'OpenOffice Formula File'
    ),
    '.odg' => array(
        'mimeTypes' => array(
            'application/vnd.oasis.opendocument.graphics'
        ),
        'label' => 'OpenOffice Graphics File'
    ),
    '.odp' => array(
        'mimeTypes' => array(
            'application/vnd.oasis.opendocument.presentation'
        ),
        'label' => 'OpenOffice Presentation File'
    ),
    '.ods' => array(
        'mimeTypes' => array(
            'application/vnd.oasis.opendocument.spreadsheet'
        ),
        'label' => 'OpenOffice Spreadsheet File'
    ),
    '.odt' => array(
        'mimeTypes' => array(
            'application/vnd.oasis.opendocument.text'
        ),
        'label' => 'OpenOffice Document'
    ),
    '.ogg' => array(
        'mimeTypes' => array(
            'application/ogg',
            'application/x-ogg',
            'audio/x-ogg'
        ),
        'label' => 'OGG Audio File'
    ),
    '.one' => array(
        'mimeTypes' => array(
            'application/msonenote'
        ),
        'label' => 'Microsoft OneNote File'
    ),
    '.p12' => array(
        'mimeTypes' => array(
            'application/x-x509-ca-cert',
            'application/pkix-cert',
            'application/x-pkcs12',
            'application/keychain_access'
        ),
        'label' => 'Internet Security Certificate File'
    ),
    '.patch' => array(
        'mimeTypes' => array(
            'text/x-patch'
        ),
        'label' => 'Patch Source File'
    ),
    '.pbm' => array(
        'mimeTypes' => array(
            'image/x-portable-bitmap',
            'image/pbm',
            'image/portable-bitmap',
            'image/x-pbm'
        ),
        'label' => 'Portable Bitmap Image'
    ),
    '.pcd' => array(
        'mimeTypes' => array(
            'image/x-photo-cd',
            'image/pcd'
        ),
        'label' => 'Kodak Photo CD File'
    ),
    '.pct' => array(
        'mimeTypes' => array(
            'image/x-pict',
            'image/pict',
            'image/x-macpict'
        ),
        'label' => 'Macintosh Quickdraw Image'
    ),
    '.pdf' => array(
        'mimeTypes' => array(
            'application/pdf',
            'application/acrobat',
            'application/nappdf',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'text/x-pdf'
        ),
        'label' => 'Adobe Acrobat File'
    ),
    '.pfx' => array(
        'mimeTypes' => array(
            'application/x-pkcs12'
        ),
        'label' => 'Personal Information Exchange File'
    ),
    '.pgm' => array(
        'mimeTypes' => array(
            'image/x-portable-graymap',
            'image/x-pgm'
        ),
        'label' => 'Portable Graymap Image'
    ),
    '.php' => array(
        'mimeTypes' => array(
            'application/x-php',
            'application/php',
            'text/php',
            'text/x-php'
        ),
        'label' => 'PHP Source File'
    ),
    '.pic' => array(
        'mimeTypes' => array(
            'image/x-pict',
            'image/pict',
            'image/x-macpict'
        ),
        'label' => 'Macintosh Quickdraw Image'
    ),
    '.pict' => array(
        'mimeTypes' => array(
            'image/x-pict',
            'image/pict',
            'image/x-macpict'
        ),
        'label' => 'Macintosh Quickdraw Image'
    ),
    '.pjpeg' => array(
        'mimeTypes' => array(
            'image/jpeg',
            'image/pjpeg'
        ),
        'label' => 'JPEG Image'
    ),
    '.pl' => array(
        'mimeTypes' => array(
            'application/x-perl',
            'text/x-perl'
        ),
        'label' => 'Perl Source File'
    ),
    '.pls' => array(
        'mimeTypes' => array(
            'audio/x-mpegurl',
            'application/x-winamp-playlist',
            'audio/mpegurl',
            'audio/mpeg-url',
            'audio/playlist',
            'audio/scpls',
            'audio/x-scpls'
        ),
        'label' => 'MP3 Playlist File'
    ),
    '.pko' => array(
        'mimeTypes' => array(
            'application/ynd.ms-pkipko'
        ),
        'label' => 'PublicKey Security Object'
    ),
    '.pm' => array(
        'mimeTypes' => array(
            'application/x-perl',
            'text/x-perl'
        ),
        'label' => 'Perl Source File'
    ),
    '.pmc' => array(
        'mimeTypes' => array(
            'application/x-perfmon'
        ),
        'label' => 'Windows Performance Monitor File'
    ),
    '.png' => array(
        'mimeTypes' => array(
            'image/png',
            'image/x-png'
        ),
        'label' => 'PNG Image'
    ),
    '.pnm' => array(
        'mimeTypes' => array(
            'image/x-portable-anymap'
        ),
        'label' => 'Portable Any Map Graphic Bitmap'
    ),
    '.pod' => array(
        'mimeTypes' => array(
            'text/x-pod'
        ),
        'label' => 'Perl Documentation File'
    ),
    '.potm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-powerpoint.template.macroEnabled.12'
        ),
        'label' => 'Microsoft PowerPoint Template File'
    ),
    '.potx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.presentationml.template'
        ),
        'label' => 'Microsoft PowerPoint Template File'
    ),
    '.ppam' => array(
        'mimeTypes' => array(
            'application/vnd.ms-powerpoint.addin.macroEnabled.12'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.ppm' => array(
        'mimeTypes' => array(
            'image/x-portable-pixmap',
            'application/ppm',
            'application/x-ppm',
            'image/x-ppm'
        ),
        'label' => 'Portable Pixmap Image'
    ),
    '.pps' => array(
        'mimeTypes' => array(
            'application/vnd.ms-powerpoint',
            'application/ms-powerpoint',
            'application/mspowerpoint',
            'application/powerpoint',
            'application/ppt',
            'application/vnd-mspowerpoint',
            'application/vnd_ms-powerpoint',
            'application/x-mspowerpoint',
            'application/x-powerpoint'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.ppsm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.ppsx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.ppt' => array(
        'mimeTypes' => array(
            'application/vnd.ms-powerpoint',
            'application/ms-powerpoint',
            'application/mspowerpoint',
            'application/powerpoint',
            'application/ppt',
            'application/vnd-mspowerpoint',
            'application/vnd_ms-powerpoint',
            'application/x-mspowerpoint',
            'application/x-powerpoint'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.pptm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-powerpoint.presentation.macroEnabled.12'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.pptx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ),
        'label' => 'Microsoft PowerPoint File'
    ),
    '.prf' => array(
        'mimeTypes' => array(
            'application/pics-rules'
        ),
        'label' => 'PICS Rules File'
    ),
    '.ps' => array(
        'mimeTypes' => array(
            'application/postscript',
            'application/eps',
            'application/x-eps',
            'image/eps',
            'image/x-eps'
        ),
        'label' => 'PostScript File'
    ),
    '.psd' => array(
        'mimeTypes' => array(
            'application/photoshop',
            'application/psd',
            'application/x-photoshop',
            'image/photoshop',
            'image/psd',
            'image/x-photoshop',
            'image/x-psd'
        ),
        'label' => 'Adobe Photoshop File'
    ),
    '.pub' => array(
        'mimeTypes' => array(
            'application/vnd.ms-publisher',
            'application/x-mspublisher'
        ),
        'label' => 'Microsoft Publisher File'
    ),
    '.py' => array(
        'mimeTypes' => array(
            'text/x-python'
        ),
        'label' => 'Python Source File'
    ),
    '.qt' => array(
        'mimeTypes' => array(
            'video/quicktime'
        ),
        'label' => 'Quicktime Video File'
    ),
    '.ra' => array(
        'mimeTypes' => array(
            'audio/vnd.rn-realaudio',
            'audio/vnd.pn-realaudio',
            'audio/x-pn-realaudio',
            'audio/x-pn-realaudio-plugin',
            'audio/x-pn-realvideo',
            'audio/x-realaudio'
        ),
        'label' => 'RealAudio File'
    ),
    '.ram' => array(
        'mimeTypes' => array(
            'audio/vnd.rn-realaudio',
            'audio/vnd.pn-realaudio',
            'audio/x-pn-realaudio',
            'audio/x-pn-realaudio-plugin',
            'audio/x-pn-realvideo',
            'audio/x-realaudio'
        ),
        'label' => 'RealAudio File'
    ),
    '.rar' => array(
        'mimeTypes' => array(
            'application/rar',
            'application/x-rar-compressed'
        ),
        'label' => 'Compressed Archive'
    ),
    '.ras' => array(
        'mimeTypes' => array(
            'image/x-cmu-raster'
        ),
        'label' => 'Raster Image'
    ),
    '.rgb' => array(
        'mimeTypes' => array(
            'image/x-rgb',
            'image/rgb'
        ),
        'label' => 'IRIS Image'
    ),
    '.rm' => array(
        'mimeTypes' => array(
            'application/vnd.rn-realmedia'
        ),
        'label' => 'RealMedia File'
    ),
    '.rmi' => array(
        'mimeTypes' => array(
            'audio/mid'
        ),
        'label' => 'Radio MIDI File'
    ),
    '.roff' => array(
        'mimeTypes' => array(
            'application/x-troff'
        ),
        'label' => 'Troff File'
    ),
    '.rpm' => array(
        'mimeTypes' => array(
            'audio/vnd.rn-realaudio',
            'audio/vnd.pn-realaudio',
            'audio/x-pn-realaudio',
            'audio/x-pn-realaudio-plugin',
            'audio/x-pn-realvideo',
            'audio/x-realaudio'
        ),
        'label' => 'RealAudio File'
    ),
    '.rtf' => array(
        'mimeTypes' => array(
            'application/rtf',
            'application/richtext',
            'application/x-rtf',
            'text/richtext',
            'text/rtf'
        ),
        'label' => 'Rich Text Format File'
    ),
    '.rtx' => array(
        'mimeTypes' => array(
            'application/rtf',
            'application/richtext',
            'application/x-rtf',
            'text/richtext',
            'text/rtf'
        ),
        'label' => 'Rich Text Format File'
    ),
    '.rv' => array(
        'mimeTypes' => array(
            'video/vnd.rn-realvideo',
            'video/x-pn-realvideo'
        ),
        'label' => 'RealVideo File'
    ),
    '.sas' => array(
        'mimeTypes' => array(
            'application/sas',
            'application/x-sas',
            'application/x-sas-data',
            'application/x-sas-log',
            'application/x-sas-output'
        ),
        'label' => 'SAS File'
    ),
    '.sav' => array(
        'mimeTypes' => array(
            'application/spss'
        ),
        'label' => 'SPSS File'
    ),
    '.scd' => array(
        'mimeTypes' => array(
            'application/x-msschedule'
        ),
        'label' => 'Schedule Data'
    ),
    '.scm' => array(
        'mimeTypes' => array(
            'text/x-script.scheme',
            'text/x-scheme'
        ),
        'label' => 'Scheme File'
    ),
    '.sct' => array(
        'mimeTypes' => array(
            'text/scriptlet'
        ),
        'label' => 'Windows Script Component'
    ),
    '.sd2' => array(
        'mimeTypes' => array(
            'application/spss'
        ),
        'label' => 'SPSS File'
    ),
    '.sea' => array(
        'mimeTypes' => array(
            'application/x-sea'
        ),
        'label' => 'Self-extracting Archive'
    ),
    '.sh' => array(
        'mimeTypes' => array(
            'application/x-sh',
            'application/x-shellscript'
        ),
        'label' => 'Shell Script File'
    ),
    '.shar' => array(
        'mimeTypes' => array(
            'application/x-shar'
        ),
        'label' => 'UNIX Shar Archive File'
    ),
    '.shtml' => array(
        'mimeTypes' => array(
            'text/html',
            'application/xhtml+xml'
        ),
        'label' => 'HTML File'
    ),
    '.sit' => array(
        'mimeTypes' => array(
            'application/stuffit',
            'application/x-sit',
            'application/x-stuffit'
        ),
        'label' => 'Stuffit Archive'
    ),
    '.smil' => array(
        'mimeTypes' => array(
            'application/smil',
            'application/smil+xml'
        ),
        'label' => 'SMIL File'
    ),
    '.snd' => array(
        'mimeTypes' => array(
            'audio/basic',
            'audio/au',
            'audio/x-au',
            'audio/x-basic'
        ),
        'label' => 'Audio File'
    ),
    '.spl' => array(
        'mimeTypes' => array(
            'application/x-shockwave-flash',
            'application/futuresplash'
        ),
        'label' => 'Adobe Flash File'
    ),
    '.spo' => array(
        'mimeTypes' => array(
            'application/spss'
        ),
        'label' => 'SPSS File'
    ),
    '.sql' => array(
        'mimeTypes' => array(
            'text/x-sql',
            'text/sql'
        ),
        'label' => 'SQL File'
    ),
    '.src' => array(
        'mimeTypes' => array(
            'application/x-wais-source'
        ),
        'label' => 'WAIS Source File'
    ),
    '.sst' => array(
        'mimeTypes' => array(
            'application/vnd.ms-pkicertstore'
        ),
        'label' => 'Certificate Store Crypto Shell Extension'
    ),
    '.stl' => array(
        'mimeTypes' => array(
            'application/vnd.ms-pkistl'
        ),
        'label' => 'Certificate Trust List'
    ),
    '.stm' => array(
        'mimeTypes' => array(
            'text/html'
        ),
        'label' => 'SHTML File'
    ),
    '.swa' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.swf' => array(
        'mimeTypes' => array(
            'application/x-shockwave-flash',
            'application/futuresplash'
        ),
        'label' => 'Adobe Flash File'
    ),
    '.sxw' => array(
        'mimeTypes' => array(
            'application/vnd.sun.xml.writer'
        ),
        'label' => 'OpenOffice.org Document'
    ),
    '.t' => array(
        'mimeTypes' => array(
            'application/x-troff'
        ),
        'label' => 'Troff File'
    ),
    '.tar' => array(
        'mimeTypes' => array(
            'application/tar',
            'application/x-gtar',
            'application/x-tar'
        ),
        'label' => 'Compressed Tar File'
    ),
    '.tcl' => array(
        'mimeTypes' => array(
            'application/x-tcl',
            'text/x-script.tcl',
            'text/x-tcl'
        ),
        'label' => 'TCL Source File'
    ),
    '.tex' => array(
        'mimeTypes' => array(
            'application/x-tex',
            'text/x-tex'
        ),
        'label' => 'TeX File'
    ),
    '.tga' => array(
        'mimeTypes' => array(
            'image/x-targa',
            'application/tga',
            'application/x-targa',
            'application/x-tga',
            'image/targa',
            'image/tga',
            'image/x-tga'
        ),
        'label' => 'Truevision Targa Image'
    ),
    '.tgz' => array(
        'mimeTypes' => array(
            'application/gzip',
            'application/gzip-compressed',
            'application/gzipped',
            'application/x-gunzip',
            'application/x-gzip'
        ),
        'label' => 'Compressed Gzip Archive'
    ),
    '.tif' => array(
        'mimeTypes' => array(
            'image/tiff',
            'application/tif',
            'application/tiff',
            'application/x-tif',
            'application/x-tiff',
            'image/tif',
            'image/x-tif',
            'image/x-tiff'
        ),
        'label' => 'Tagged Image File'
    ),
    '.tiff' => array(
        'mimeTypes' => array(
            'image/tiff',
            'application/tif',
            'application/tiff',
            'application/x-tif',
            'application/x-tiff',
            'image/tif',
            'image/x-tif',
            'image/x-tiff'
        ),
        'label' => 'Tagged Image File'
    ),
    '.tnef' => array(
        'mimeTypes' => array(
            'application/ms-tnef'
        ),
        'label' => 'Microsoft Exchange TNEF File'
    ),
    '.tr' => array(
        'mimeTypes' => array(
            'application/x-troff'
        ),
        'label' => 'Troff File'
    ),
    '.trm' => array(
        'mimeTypes' => array(
            'application/x-msterminal'
        ),
        'label' => 'Terminal Settings'
    ),
    '.tsv' => array(
        'mimeTypes' => array(
            'text/tsv',
            'text/tab-separated-values',
            'text/x-tab-separated-values'
        ),
        'label' => 'Tab-delimited File'
    ),
    '.twb' => array(
        'mimeTypes' => array(
            'application/twb',
            'application/twbx',
            'application/x-twb'
        ),
        'label' => 'Tableau Workbook File'
    ),
    '.twbx' => array(
        'mimeTypes' => array(
            'application/twb',
            'application/twbx',
            'application/x-twb'
        ),
        'label' => 'Tableau Workbook File'
    ),
    '.txt' => array(
        'mimeTypes' => array(
            'text/plain'
        ),
        'label' => 'Plain Text File'
    ),
    '.uls' => array(
        'mimeTypes' => array(
            'text/iuls'
        ),
        'label' => 'Internet Location Service'
    ),
    '.ustar' => array(
        'mimeTypes' => array(
            'application/x-ustar'
        ),
        'label' => 'POSIX Tar Compressed Archive'
    ),
    '.vcf' => array(
        'mimeTypes' => array(
            'text/x-vcard'
        ),
        'label' => 'VCard File'
    ),
    '.vrml' => array(
        'mimeTypes' => array(
            'x-world/x-vrml'
        ),
        'label' => 'Virtual Reality Modeling Language File'
    ),
    '.vsd' => array(
        'mimeTypes' => array(
            'application/vnd.visio',
            'application/visio',
            'application/visio.drawing',
            'application/vsd',
            'application/x-visio',
            'application/x-vsd',
            'image/x-vsd'
        ),
        'label' => 'Microsoft Visio File'
    ),
    '.w3d' => array(
        'mimeTypes' => array(
            'application/x-director'
        ),
        'label' => 'Adobe Director File'
    ),
    '.war' => array(
        'mimeTypes' => array(
            'application/x-webarchive'
        ),
        'label' => 'KDE Web Archive'
    ),
    '.wav' => array(
        'mimeTypes' => array(
            'audio/wav',
            'audio/s-wav',
            'audio/wave',
            'audio/x-wav'
        ),
        'label' => 'Waveform Audio File'
    ),
    '.wcm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-works'
        ),
        'label' => 'Works File Transmission'
    ),
    '.wdb' => array(
        'mimeTypes' => array(
            'application/vnd.ms-works',
            'application/x-msworks-wp'
        ),
        'label' => 'Microsoft Works File'
    ),
    '.wks' => array(
        'mimeTypes' => array(
            'application/vnd.ms-works',
            'application/x-msworks-wp'
        ),
        'label' => 'Microsoft Works File'
    ),
    '.wma' => array(
        'mimeTypes' => array(
            'audio/x-ms-wma'
        ),
        'label' => 'Windows Media File'
    ),
    '.wmf' => array(
        'mimeTypes' => array(
            'image/x-wmf',
            'application/wmf',
            'application/x-msmetafile',
            'application/x-wmf',
            'image/wmf',
            'image/x-win-metafile'
        ),
        'label' => 'Windows Media File'
    ),
    '.wmv' => array(
        'mimeTypes' => array(
            'video/x-ms-wmv'
        ),
        'label' => 'Windows Media File'
    ),
    '.wmz' => array(
        'mimeTypes' => array(
            'application/x-ms-wmz'
        ),
        'label' => 'Windows Media Compressed File'
    ),
    '.wpd' => array(
        'mimeTypes' => array(
            'application/wordperfect',
            'application/wordperf',
            'application/wpd'
        ),
        'label' => 'WordPerfect Document'
    ),
    '.wps' => array(
        'mimeTypes' => array(
            'application/vnd.ms-works',
            'application/x-msworks-wp'
        ),
        'label' => 'Microsoft Works File'
    ),
    '.wri' => array(
        'mimeTypes' => array(
            'application/x-mswrite'
        ),
        'label' => 'Write Document'
    ),
    '.wrl' => array(
        'mimeTypes' => array(
            'x-world/x-vrml'
        ),
        'label' => 'VRML 3D File'
    ),
    '.wrz' => array(
        'mimeTypes' => array(
            'x-world/x-vrml'
        ),
        'label' => 'VRML 3D File'
    ),
    '.xbm' => array(
        'mimeTypes' => array(
            'image/x-xbitmap'
        ),
        'label' => 'Bitmap Image'
    ),
    '.xhtml' => array(
        'mimeTypes' => array(
            'text/html',
            'application/xhtml+xml'
        ),
        'label' => 'HTML File'
    ),
    '.xla' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel'
        ),
        'label' => 'Excel Add-in'
    ),
    '.xlam' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel.addin.macroEnabled.12'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xlc' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel'
        ),
        'label' => 'Excel Chart'
    ),
    '.xll' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel',
            'application/excel',
            'application/msexcel',
            'application/msexcell',
            'application/x-dos_ms_excel',
            'application/x-excel',
            'application/x-ms-excel',
            'application/x-msexcel',
            'application/x-xls',
            'application/xls'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xlm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel'
        ),
        'label' => 'Excel Macro File'
    ),
    '.xls' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel',
            'application/excel',
            'application/msexcel',
            'application/msexcell',
            'application/x-dos_ms_excel',
            'application/x-excel',
            'application/x-ms-excel',
            'application/x-msexcel',
            'application/x-xls',
            'application/xls'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xlsb' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel.sheet.binary.macroEnabled.12'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xlsm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel.sheet.macroEnabled.12'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xlsx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xlt' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel'
        ),
        'label' => 'Excel Template File'
    ),
    '.xltm' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel.template.macroEnabled.12'
        ),
        'label' => 'Microsoft Excel File'
    ),
    '.xltx' => array(
        'mimeTypes' => array(
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template'
        ),
        'label' => 'Microsoft Excel Template File'
    ),
    '.xlw' => array(
        'mimeTypes' => array(
            'application/vnd.ms-excel'
        ),
        'label' => 'Excel Workspace File'
    ),
    '.xml' => array(
        'mimeTypes' => array(
            'text/xml',
            'application/x-xml',
            'application/xml'
        ),
        'label' => 'XML File'
    ),
    '.xpm' => array(
        'mimeTypes' => array(
            'image/x-xpixmap',
            'image/x-xpm',
            'image/xpm'
        ),
        'label' => 'Pixmap Image'
    ),
    '.xps' => array(
        'mimeTypes' => array(
            'application/vnd.ms-xpsdocument'
        ),
        'label' => 'Microsoft XPS File'
    ),
    '.xsl' => array(
        'mimeTypes' => array(
            'text/xsl'
        ),
        'label' => 'XSLT Stylesheet File'
    ),
    '.xwd' => array(
        'mimeTypes' => array(
            'image/x-xwindowdump',
            'image/xwd',
            'image/x-xwd',
            'application/xwd',
            'application/x-xwd'
        ),
        'label' => 'X Windows Dump'
    ),
    '.z' => array(
        'mimeTypes' => array(
            'application/x-compress',
            'application/z',
            'application/x-z'
        ),
        'label' => 'UNIX Compressed Archive File'
    ),
    '.zip' => array(
        'mimeTypes' => array(
            'application/zip',
            'application/x-compress',
            'application/x-compressed',
            'application/x-zip',
            'application/x-zip-compressed',
            'application/zip-compressed',
            'application/x-7zip-compressed'
        ),
        'label' => 'Compressed Zip Archive'
    ),
);