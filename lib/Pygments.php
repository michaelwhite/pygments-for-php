<?php
/**
 * Pygments for PHP is a PHP-based wrapper to some of the Python Pygments API via the command-line pygmentize script.
 *
 * @date 2013-02-05
 * @copyright Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 *
 * Bitbucket URL: http://bitbucket.org/markswhitemedia/pygments-for-php
 *
 * BSD 3-Clause License
 *
 * Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 *      disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * - Neither the name of the Michael White nor the names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * Provides a PHP-based wrapper to some of the Python Pygments API via the command-line pygmentize script.
 *
 * @package Pygments
 */
class Pygments {
    protected static $_lexers;
    protected static $_formatters;

    protected $_code = '';
    protected $_language = '';
    protected $_style = '';
    protected $_tabWidth = 4;
    protected $_options = array();

    /**
     * Allows the Python method names to be used by stripping underscores from the method names.
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($methodName, $arguments) {
        // If the call uses an underscore after the first character, strip the underscores and then call the method.
        // Since PHP treats method names as case-insensitive we don't have to worry about camel-casing them to make the call.
        if(strpos($methodName, '_', 1) !== false) {
            $methodName = str_replace('_', '', $methodName);
            $targetCall = array($this, $methodName);
            if(is_callable($targetCall)) {
                return call_user_func_array($targetCall, $arguments);
            }
        }
        return null;
    }

    /**
     * Attempts to highlight the provided code by leveraging the Pygments API via the pygmentize command-line script.
     *
     * Example: (input file is C++ code, output is formatted for HTML display, line numbers shown in table cell, and a full HTML document will be rendered)
     *      pygmentize -l cpp -f html -O linenos=table,full=1
     *
     * The code to make the above command-line call would look like:
     * $pygments = new Pygments();
     * $pygments->setOptions(array(
     *      'linenos' => 'table',
     *      'full' => true
     * ));
     * $pygments->highlight($cppCode, 'cpp', 'html');
     *
     * @param string $code The source code to highlight.
     * @param string $lexerName Any valid Pygments lexer. Use getAllLexers() to see available lexer names.
     * @param string $formatterName Any valid Pygments lexer. Use getAllFormatters() to see available formatter names.
     * @param string $targetFile A path to a file writable by the current PHP process. If blank a temporary file will be used.
     * @return string
     */
    public function highlight($code, $lexerName, $formatterName, $targetFile='') {

        // Clean up external input that will be fed directly to the shell
        $lexerName = escapeshellarg($lexerName);
        $formatterName = escapeshellarg($formatterName);

        // Create a temporary file
        if(!is_writable($targetFile)) {
            $targetFile = tempnam('/tmp', 'pygmentize_');
            file_put_contents($targetFile, $code);
        }

        if($lexerName) {
            $lexerName = ' -l '.$lexerName;
        }

        if($formatterName) {
            $formatterName = ' -f '.$formatterName;
        }

        $options = $this->_options;
        if(count($options)) {
            foreach($options as $optionName => $optionValue) {
                $options[$optionName] = $optionName.'='.$optionValue;
            }
            $options = ' -O '.implode(',', $options).' ';
        }

        $command = 'pygmentize'.$formatterName.$lexerName.$options.$targetFile;
        $output = array();
        $returnValue = -1;

        exec($command, $output, $returnValue);

        $output = implode("\n", $output);

        return $output;
    }

    /**
     * Sets an array of options to pass into the pygmentize command-line script. These are passed as part of the -O flag.
     *
     * Example: (input file is C++ code, output is formatted for HTML display, line numbers shown in table cell, and a full HTML document will be rendered)
     *      pygmentize -l cpp -f html -O linenos=table,full=1 test-highlight.cpp
     *
     * The options for the above command-line call would look like this in PHP:
     * $options = [
     *      'linenos' => 'table',
     *      'full' => true
     * ];
     *
     * @param array $options
     */
    public function setOptions($options) {
        $this->_options = (array) $options;
    }

    /**
     * Returns the full list of available lexers. If needed, it will use output from pygmentize -L to generate the list.
     *
     * @return array
     */
    public function getAllLexers() {
        return self::_loadLexers();
    }

    /**
     * Returns the full list of available formatters. If needed, it will use output from pygmentize -L to generate the
     * list.
     *
     * @return array
     */
    public function getAllFormatters() {
        return self::_loadFormatters();
    }

    /**
     * Attempts to match the file extension to a file pattern recognized by an available lexer.
     *
     * @param $fileName
     * @return string|null
     */
    public function getLexerForFileName($fileName) {
        $filePattern = '*'.\Pygments\Mime::getFileExtension($fileName);
        $lexers = $this->getAllLexers();
        foreach($lexers as $lexer) {
            if(in_array(strtolower($filePattern), $lexer['filePatterns'])) {
                return array_shift($lexer['shortNames']);
            }
        }
        return null;
    }

    /**
     * Alias of getLexerForFileName(). Currently, text is ignored as we have no straight-forward and intelligent process
     * for making use of it.
     *
     * NOTE: It is preferred that you know what kind of content you are dealing with or that you can at least rely on
     *       the file extension with certainty.
     *
     * @param $fileName
     * @param $text
     * @return string|null
     */
    public function guessLexerForFileName($fileName, $text) {
        return $this->getLexerForFileName($fileName);
    }

    /**
     * Attempts to look up the file extension that matches the mime type provided and then uses that file extension to
     * determine the lexer.
     *
     * NOTE: This lookup may always be somewhat unstable. Mime-types are often used for many different file extensions.
     *
     * @param $mimeType
     * @return string|null
     */
    public function getLexerForMimeType($mimeType) {
        $filePath = 'fake'.\Pygments\Mime::getExtensionByMimeType($mimeType);
        return self::getLexerForFileName($filePath);
    }

    /*

    TODO - Implement these methods as possible and as time allows.

    public function getLexerByName($alias)

    public function guessLexer($text)

    public function guessLexerForFileName($fileName, $text)

    public function getFormatterByName($alias)

    public function getFormatterForFileName($fileName)

    public function getAllStyles()

    */

    /**
     * Attempts to load the lexers list by any means available if the list has not already been loaded.
     *
     * @return array
     */
    protected static function _loadLexers() {
        if(is_array(self::$_lexers)) {
            return self::$_lexers;
        }

        $lexerJsonFile = __DIR__.DIRECTORY_SEPARATOR.'Pygments'.DIRECTORY_SEPARATOR.'lexers.json';

        // Load and decode the JSON file if we have it, otherwise try to rebuild it (which also generates the raw lexers array)
        if(is_file($lexerJsonFile)) {
            self::$_lexers = json_decode(file_get_contents($lexerJsonFile), true);
        } else {
            self::_rebuildLexerJsonFile();
        }

        return self::$_lexers;
    }

    /**
     * Attempts to load the formatters list by any means available if the list has not already been loaded.
     *
     * @return array
     */
    protected static function _loadFormatters() {
        if(is_array(self::$_formatters)) {
            return self::$_formatters;
        }

        $formatterJsonFile = __DIR__.DIRECTORY_SEPARATOR.'Pygments'.DIRECTORY_SEPARATOR.'formatters.json';

        // Load and decode the JSON file if we have it, otherwise try to rebuild it (which also generates the raw formatters array)
        if(is_file($formatterJsonFile)) {
            self::$_formatters = json_decode(file_get_contents($formatterJsonFile), true);
        } else {
            self::_rebuildFormatterJsonFile();
        }
        return self::$_formatters;
    }

    /**
     * Attempts to backup and rebuild the stored Lexer JSON data file from the command-line output of pygmentize -L
     *
     * @return bool Whether or not the file was rebuilt successfully.
     */
    protected static function _rebuildLexerJsonFile() {
        self::$_lexers = self::_loadAndParsePygmentizeLexers();

        $fileName = __DIR__.DIRECTORY_SEPARATOR.'Pygments'.DIRECTORY_SEPARATOR.'lexers.json';

        $bakFileName = $fileName.'.bak';
        if(is_file($bakFileName)) {
            unlink($bakFileName);
        }
        if(is_file($fileName)) {
            copy($fileName, $bakFileName);
        }
        $jsonString = json_encode(self::$_lexers);
        return (bool) file_put_contents($fileName, $jsonString);
    }

    /**
     * Attempts to backup and rebuild the stored Formatter JSON data file from the command-line output of pygmentize -L
     * 
     * @return bool Whether or not the file was rebuilt successfully.
     */
    protected static function _rebuildFormatterJsonFile() {
        self::$_formatters = self::_loadAndParsePygmentizeFormatters();

        $fileName = __DIR__.DIRECTORY_SEPARATOR.'Pygments'.DIRECTORY_SEPARATOR.'formatters.json';

        $bakFileName = $fileName.'.bak';
        if(is_file($bakFileName)) {
            unlink($bakFileName);
        }
        if(is_file($fileName)) {
            copy($fileName, $bakFileName);
        }
        $jsonString = json_encode(self::$_formatters);
        return (bool) file_put_contents($fileName, $jsonString);
    }

    /**
     * Parses command-line output to build a list of all available pygmentize lexers.
     *
     * Uses the command-line pygmentize script to generate a list of lexers and their meta-data and creates a usable
     * PHP array with the data.
     *
     * @return array
     */
    protected static function _loadAndParsePygmentizeLexers() {
        $command = "pygmentize -L";
        $outputLines = array();
        $returnValue = -1;
        exec($command, $outputLines, $returnValue);

        $lexers = array();

        // Relies on the output being in a very specific format
        $foundLexers = false;
        $totalLines = count($outputLines);
        for($i=0; $i < $totalLines; $i++) {
            $line = $outputLines[$i];

            // Does this line contain the lexers label?
            if(!$foundLexers && strpos($line, 'Lexers:') > -1) {
                // Skip the next line since it is just ~~~~~~
                $i++;
                $foundLexers = true;
                continue;
            } elseif($foundLexers) {
                if(strpos($line, '*') === 0) { // We are past the Lexers header, make sure the line begins with an * character
                    $i++;
                    $nextLine = $outputLines[$i];
                    $shortNames = explode(',', trim($line, '*: '));
                    if(strpos($nextLine, '(') > -1) {
                        list($name, $filePatternsString) = explode('(filenames', trim($nextLine));
                        $filePatterns = explode(',', $filePatternsString);
                    } else {
                        $name = trim($nextLine);
                        $filePatterns = array();
                    }
                    $lexers[$name] = array(
                        'name' => $name,
                        'shortNames' => $shortNames,
                        'filePatterns' => $filePatterns
                    );
                    continue;
                } else {
                    // We are done parsing the lexers list, no more looping needed.
                    break;
                }
            }
            // Do nothing this loop, still looking for the lexers label...
        }

        return $lexers;
    }

    /**
     * Parses command-line output to build a list of all available pygmentize formatters.
     *
     * Uses the command-line pygmentize script to generate a list of formatters and their meta-data and creates a usable
     * PHP array with the data.
     * 
     * @return array
     */
    protected static function _loadAndParsePygmentizeFormatters() {
        $command = "pygmentize -L";
        $outputLines = array();
        $returnValue = -1;
        exec($command, $outputLines, $returnValue);

        $formatters = array();

        // Relies on the output being in a very specific format
        $foundFormatters = false;
        $totalLines = count($outputLines);
        for($i=0; $i < $totalLines; $i++) {
            $line = $outputLines[$i];

            // Does this line contain the formatters label?
            if(!$foundFormatters && strpos($line, 'Formatters:') > -1) {
                // Skip the next line since it is just ~~~~~~
                $i++;
                $foundFormatters = true;
                continue;
            } elseif($foundFormatters) {
                if(strpos($line, '*') === 0) { // We are past the Formatters header, make sure the line begins with an * character
                    $i++;
                    $descriptionLine = $outputLines[$i];
                    $formatterNames = explode(',', $line);
                    foreach($formatterNames as $formatterName) {
                        $formatterName = trim($formatterName);
                        $formatters[$formatterName] = array(
                            'name' => $formatterName,
                            'aliases' => $formatterNames,
                            'description' => trim($descriptionLine)
                        );
                    }
                    continue;
                } else {
                    // We are done parsing the formatters list, no more looping needed.
                    break;
                }
            }
            // Do nothing this loop, still looking for the formatters label...
        }

        return $formatters;
    }
}