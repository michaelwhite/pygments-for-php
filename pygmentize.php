<?php
/**
 * Pygments for PHP is a PHP-based wrapper to some of the Python Pygments API via the command-line pygmentize script.
 *
 *
 * ABOUT THIS SCRIPT
 * - This file acts as an API service script for obtaining highlighted code.
 * - Currently this API service does not provide any protection options for preventing use outside the serving domain.
 * - See pygmentize-readme.txt for instructions on how to use this API.
 *
 *
 * @date 2013-02-05
 * @copyright Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 *
 * Bitbucket URL: http://bitbucket.org/markswhitemedia/pygments-for-php
 *
 * BSD 3-Clause License
 *
 * Copyright (c) 2013, Michael White <pygments@markswhitemedia.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 *      disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * - Neither the name of the Michael White nor the names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once('load-pygments.php');

$formData = $_REQUEST;

// Set the lexer name we want to use. In this case, we are trying to highlight some HTML code.
$lexerName = @$formData['lexer'] ?: 'html'; // Default to HTML for the code lexer (aka code language)

// Set the formatter name we want to use. In this case, we are rendering the highlighted code inside an HTML document.
$formatterName = 'html'; // @todo - make this a setting controlled by a post variable

// Get an instance of the Pygments class.
$pygments = new Pygments();

// Set up a few standard options
$pygments->setOptions(array(
    'linenos' => 'table',   // Render line numbers in a table cell. (@todo - make this a setting controlled by a post variable)
    'full' => 0,            // Do not render a full HTML document (@todo - make this a setting controlled by a post variable)
    'linespans' => 'code'   // Will not be available unless the server running this script has Pygments 1.6+ installed. (@todo - make this a setting controlled by a post variable)
));

// Get the highlighted code!
$result = $pygments->highlight(@$formData['code'], $lexerName, $formatterName);

$message = array(
    'result' => $result
);

// Encode json data...
$jsonData = json_encode($message);

// Found some cases where json_encode() does not process the data properly when there are multi-byte or other non-ASCII characters in the content.
// Checking for and handling this case costs about 0.0003 seconds per JSON render when the content is fairly long.
$decodeCheck = json_decode($jsonData);
if((function_exists('mb_strlen') && (mb_strlen($decodeCheck->result) != mb_strlen($message['result']))) || (strlen($decodeCheck->result) != strlen($message['result']))) {
    $message['result'] = 'Encoding error on server. Currently this system expects ASCII or UTF-8 characters.';
    $jsonData = json_encode($message);
}

header('Content-type: application/json');
echo $jsonData;
exit;